\documentclass[10pt]{article}
\usepackage{graphicx,pxfonts,ulem,color,cancel}

\textwidth= 468pt
\textheight= 648pt
\topmargin = -36pt
\oddsidemargin = 0pt
\evensidemargin = 0pt
\begin{document}

\begin{center}



{\bf \Large Specification of the Inputs to the Photon Monte Carlo in the ImSim
Framework}

\vspace{0.5cm}
{John R. Peterson (Purdue), Garrett Jernigan (Berkeley), and Andy Connolly (UW)}

\vspace{0.5cm}

{Version 4.0 (Draft for DC 3b PT2), June 2010}
\end{center}

\vspace{0.5cm}

This is intended to be a working document for the specification of the basic
input of the LSST End to End Simulator.  The photon Monte Carlo portion of the simulator turns catalogs of
objects into photons and collects the photons into images.  This document
explains the specifications of the input to the photon Monte Carlo.  The input consists
of three parts:

\begin{itemize}

\item{{\bf Instance Catalog}:  A list of all astronomical objects that are
  within a {\it 2.1 degree radius circle} (which covers the full focal plane) at the
  particular time of the {\it exposure pair}.  There are many parameters that can
  describe the object that we list below.  The catalog could consist of lists of postage stamp
  truth images as well as parameterized models.}

\item{{\bf Spectral Energy Distribution (SED) files}:  A reference set of SED files to be referenced in the
  instance catalog.  Since the number of objects in a typical instance catalog
  can be around 10 million, there cannot be a different SED for every object.
  Similarly, the redshifting of photons have to be done in the simulator, so
  these are rest frame SEDs.}

\item{{\bf Observing parameters:} These are parameters defining the pointing,
  position of moon and sun, etc. that are typically taken from OpSim
  simulations.  The intention is to only worry about time during the exposure
  pair in the raytrace, and that the details about when the observation is
  taken, precession, etc. be left outside the simulator.  These parameters tend
  to be similar to the types of controls a telescope operator would specify,
  and parameters affected by the time of observation. }

\end{itemize}

\noindent
With the inputs described above the photon Monte Carlo will generate images
for the full exposure pair covering all chips.  This is 378 chips or 6048
amplifier images.  Since DC3b it is also customary to generate the electron
images (eimage) which consist of the images before detector defects are
applied and before the chips are split up into amplifiers. 

\section*{Input Specification}

\vspace{0.5cm}
\noindent
{\bf Instance Catalog:} The catalog of objects should be an ASCII
file and have the following form.  The parameters will be read in at single
precision, if that is found necessary for any of these parameters.  Each line
should contain one model, but a single astronomical objects may be composed of
multiple models.

\vspace{0.5cm}
\noindent
{\bf
  object~~ID\#~~ra~~dec~~mag~~SED\_filename~~redshift~~$\gamma_1$~~$\gamma_2$~~$\mu$~~$\Delta ra$~~$\Delta dec$~~spatial\_model\_name

spatial\_par\_1~~spatial\_par\_2~~...~~rest\_frame\_dust\_model~~dust\_par\_1~~dust\_par\_2

lab\_frame\_dust\_model~~dust\_par\_1~~dust\_par\_2}

\vspace{0.5cm}
\noindent
An entry in the catalog is actually a command
to the Monte Carlo as well, which begins with the word object.  We define each entry as follows:

\begin{itemize}

\item{ ID\#:  A floating point number to keep track of the object, which is
unused by the photon Monte Carlo.}

\item{ra:  The right ascension of the center of the object or image in decimal
  radians.}

\item{dec: The declination of the center of the object in decimal radians in
  J2000 coordinates.}

\item{mag:  The normalization of the flux of the object.  The units of
  the flux internal to the monte carlo are photons/cm$^2$/s, but we use AB
  magnitudes at 5000 $\AA$/(1+z) (which is roughly equivalent to V (AB) or g (AB)).}

\item{SED\_file:  The name of the SED file relative to the data directory.


 }


\item{redshift:  The redshift (or blueshift) of the object.  Given that redshift is
used quite often, it does not make sense to redshift the SED file externally.
The wavelength of the photons are shifted by (1+$z$) within the photon Monte
Carlo.  Any cosmological dimming must be included in the flux normalization.}

\item{ $\gamma_1$:  The value of the shear parameter $\gamma_1$ used in weak lensing.}

\item{ $\gamma_2$:  The value of the shear parameter $\gamma_2$ used in weak lensing.}

\item{ $\mu$:  The value of the magnification parameter given in magnitudes.}

\item{ $\Delta \alpha$:  The value of the declination offset in radians.  This
  can be used either for weak lensing or objects that moved from another
  exposure if you do not want to change the source position in the first two columns.}

\item{ $\Delta \delta$:  The value of the declination offset in radians.  This
  can be used either for weak lensing or objects that moved from another
  exposure if you do not want to change the source position in the first two columns.}

\item{spatial\_modelname:   The name of the spatial model to be used, which
  are coded in the Monte Carlo.    We have currently implemented the following spatial
  models:  

  \begin{itemize}
  \item{point (no parameters):  This is a model primarily used for stars, but
  also unresolved objects.}

  \item{gaussian (sigma in arcseconds):  This is a model for a
  gaussian-shaped object.}

  \item{galaxy (ratio of bulge to total, radius of disk in pixels,
  radius of bulge in pixels, rotation angle of disk to plane of sky, position angle of disk):  This is a model for a galaxy using a linear superposition
  of a spherical bulge with a sersic profile (n=4) and a exponential disk (n=1)with two
  orientation angles.}

  \item{ movingpoint (the derivative of the velocity arcseconds per
  second along the ra direction, the derivative of the velocity in arcseconds
  per second along the dec direction)} 

  \item{ sersic2D: (size of axis 1 in arcseconds, size of axis 2
  in arcseconds, position angle in radians, sersic index)}

  \item{ sersic: (size of axis 1 in arcseconds, size of axis 2
  in arcseconds, size of axis 3 in arcseconds, sersic index, polar angle in
  radians, position angle in radians)}

  \item{image models:  If the
  spatial model name is a string which contains ``fits'' or ``fit'' the model
  is assumed to be a spatial image.  Then there are two parameters for this
  model:  pixel size (in arcseconds) and rotation angle (in degrees).}
   \end{itemize}  

}

\item{spatial\_parameters:   The associated parameters for each spatial model.
There could be none or many.  While the parser is reading the model it looks
for more parameters based on the name of the model.}

\item{ Rest\_Frame\_Dust\_modelname:  This is either the ccm for the CCM model,
or calzetti for the calzetti model.  If no dust model is desired, then put
``none'' for this field.}

\item{ dust\_parameters:   The parameters for both the calzetti and CCM are the
$A_v$ followed by the $R_v$ value.  If no dust model is used, do not use parameters}

\item{ Lab\_Frame\_Dust\_modelname:  This is either the ccm for the CCM model,
or calzetti for the calzetti model.  If no dust model is desired, then put
``none'' for this field.}

\item{ dust\_parameters:   The parameters for both the calzetti and CCM are the
$A_v$ followed by the $R_v$ value.  If no dust model is used, do not use parameters}

\end{itemize}


\vspace{0.5cm}
\noindent
{\bf Non-standard ways of using this format:}  We have also previously considered the concept of a data
cube consisting of a series of images in small wavelength bands (several times
smaller than a filter width).  This approach is not optimal in using a photon
Monte Carlo for many sources, since this quickly grows too large because areas where there are
no sources have many data points.  However, for certain applications it may be
useful.  Technically, this format is supported in two different ways.  First,
one can use a series of images for a single source and have each one have a
piece of the SED.  Second, one can represent the source by a series of closely
spaced point sources each with their own SED.  

It is also possible to use a single truth image and a single SED for the entire
field, and therefore just have a single entry of the catalog.  This does not
take advantage of the wavelength-dependence capabilities in the Monte Carlo,
but can easily work.  In practice it is probably necessary to break the image
up into pieces since an full field image would be extremely large.

Several catalogs can also be obviously merged to construct larger catalogs. If
one catalog of stars is made by one person, it will be easy to merge it with a
catalog of galaxies with another person.

\vspace{0.5cm}
\noindent
{\bf SED files}:  The SED files have been found to be a critical part of
making the large scale simulation run efficiently.  The SED files are ASCII
files containing two columns: wavelength in $\AA$ and flux in units of
ergs/cm$^2$/s/$\AA$.  Any wavelength spacing is allowed and it can begin or
end at any wavelength.  The flux is renormalized as the file is read in, so
its normalization is arbitrary. Linear interpolation will be used and the SED
can be gridded to arbitrary accuracy.  The photon Monte Carlo will run much
more efficiently if files have the following properties, however:

\begin{itemize}

\item{Number of Files:  This should be kept to a reasonable number (~10,000)
  and no files should be included in the directory if it is not used by any
  object in the sky.}

\item{Wavelength Range:  The photon Monte Carlo uses photons between 300 and
  1200 nm.  There is no need to have lines in the SED outside that range with
  the exception of some of the UV for high redshift galaxies.}

\item{Wavelength Spacing:  Some of the models of throughput efficiency are
  specified on a 1 nm grid.  Although there is some interpolation, specifying
  an SED on a much finer grid is unnecessary, and will increase the simulation
  time as well as the memory requirements.  Therefore, we expect that
  resolution finer than 0.25 nm is unnecessary.}

\item{Numerical Precision:  The wavelength and the flux should not contain
  more than single precision.}

\end{itemize}


\vspace{0.5cm}
\noindent
{\bf Observation Parameters}: Observing parameters that need to be
specified to run the simulation.  The following set are currently the list
that was passed at the top of the instance catalog from DC3b PT1.  The ones
that are currently used by the photon Monte Carlo are in bold:

\vspace{0.1cm}

{\bf Unrefracted\_RA}

{\bf Unrefracted\_Dec}

{\bf Unrefracted\_Azimuth}

{\bf Unrefracted\_Altitude}

Refracted\_Airmass

{\bf Slalib\_date}

Slalib\_expMjd

{\bf SIM\_SEED}

Opsim\_fieldradeg

Opsim\_fielddecdeg

Opsim\_seeing

Opsim\_darkbright

Opsim\_fldfltrint

Opsim\_rscatter

{\bf Opsim\_rotskypos}

Opsim\_xparency

Opsim\_lst

{\bf Opsim\_obshistid}

Opsim\_propid

Opsim\_fldvisits

Opsim\_seqnnum

Opsim\_slewtime

Opsim\_subseq z

Opsim\_slewdist

{\bf Opsim\_rottelpos}

Opsim\_miescatter

Opsim\_moonillum

Opsim\_proprank

Opsim\_finrank

Opsim\_vskybright

{\bf Opsim\_moondec}

{\bf Opsim\_rawseeing}

{\bf Opsim\_moonra}

Opsim\_phaseangle

Opsim\_maxseeing

Opsim\_cldseeing

Opsim\_fldint

{\bf Opsim\_expmjd}

{\bf Opsim\_moonalt}

Opsim\_exptime

{\bf Opsim\_sunalt}

Opsim\_sunaz

{\bf Opsim\_moonphase}

Opsim\_m5sigma

Opsim\_m5sigma\_ps

{\bf Opsim\_filter}

Opsim\_filtsky

{\bf Opsim\_dist2moon}

Opsim\_pairnum

Opsim\_fieldid

Opsim\_perry\_skybrightness

\section*{Unresolved Issues:}

\begin{itemize}

\item{Are the units of everything correct?  (Probably not)}

\item{Are we going to have objects that change their brightness during the
  exposure pair?  If so, I would like the instance catalog to apply during the
  exposure pair, and then add a column that is the change in magnitude per
  unit time $\frac{\Delta m}{dt}$.  The flux norm would then be the magnitude of
  the object in the middle of the exposure pair (during the middle of the
  first readout).}

\item{Do we have to change the flux norm?  I couldn't find a single SED that
  had a value of zero at 5000 angstroms.  In addition, I am also arguing that
  the wavelength grid should become more coarse.  Is this now a non-issue?}

\item{Should we switch either the SEDs or the instance catalog to gzipped fits
  files?  This would save a factor of three in file size, but I would
  initially argue no, and we should get it working in ASCII first and the worry about
  it later after some of these changes are implemented.}

\item{We could update and simplify the observing parameters from OpSim as
  clearly many aren't needed.}

\end{itemize}




\end{document}

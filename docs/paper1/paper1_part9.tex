%-------------------------------------------------------------------------------\section*{Compare} 

\section{Comparison with data}
\label{sec:Compare}
Here we show an examples of our attempt to compare the atmospheric PSF predicted by the \phosim 
model to that from real data -- in this example we focus on comparing the PSF ellipticity and the spatial 
correlation of the PSF ellipticities. An exact and statistically meaningful comparison between 
existing data and simulations is difficult due to the following reasons: First, different instruments have 
different optics effects entangled with the atmospheric features in the PSF. Second, the detailed 
atmospheric condition (structure function and wind at different altitudes) at the time the image is taken 
is usually unknown. Finally, there exist only a limited number of wide-field, intermediate exposure time 
datasets that contain large enough number of stars to sufficiently sample the PSFs for this study. 
  
\subsection{Data} 
We choose to use the ``main'' dataset described in
%\citet{2011arXiv1110.4913H}
for this study.
This dataset consists of images taken by the 4-meter CFHT MegaCam, with 1 degree$^{2}$ 
field-of-view. The mosaic focal plane of MegaCam consists of 36 CCD sensors (with 
$12.70' \times 6.35'$ field for each chip and $0.186''$ pixels) tiled in a 9$\times$4 configuration. 
There are 60 images are taken on a dense stellar fields with exposure time $t=$ 74 seconds and 
stellar density $\approx 7 {\rm stars/arcminute}^{2}$. Table 1 in
%\citet{2011arXiv1110.4913H}
provides other details of the dataset.

We use the catalogs provided by the authors of
%\citet{2011arXiv1110.4913H}
for this study, which has been processed through the standard CFHT image pipeline and have simple shape 
measurements ($\varepsilon_{1}, \varepsilon_{2}$) for each star. In addition, according to 
%\citet{2011arXiv1110.4913H}, the large optics contribution in the PSF ellipticities need to be removed 
in order to extract the atmospheric PSF information, and a 2nd-order chip-wise polynomial fitted to the 
ellipticities and subtracted in the catalogs for this purpose. Visually, the subtraction of the polynomial 
model removes most of the large scale optics variation, but it is noted in the paper that this can also 
remove some large scale variation from the atmosphere.Examples of these residual ellipticity 
maps (single-component measured ellipticity subtracted by the 2nd-order chip-wise polynomial model 
of the ellipticity spatial variation) are shown in the top row of \Fref{fig:CFHT_maps}.


\subsection{Simulation}
        
We simulated 100 LSST 15-second exposures, each consists of $4\times 4$ LSST CCDs, which tile 
up to approximately 1 degree$^{2}$. We use the seeing distribution from the dataset (the PSF size 
distribution of all the 60 74-second exposures combined) and randomly sampling the rest of the 
atmospheric parameters to generate the 100 exposures. An over-dense stellar field of 
$\approx 7 {\rm stars/arcminute}^{2}$ was simulated to match the data.  

Each simulated mosaic image was then trimmed into $9 \times 4$ ``virtual chips'' to match the CCD size 
of MegaCam. The ellipticity of the simulated stellar images were measured and a 2nd-order polynomial 
model for ellipticity measurements were subtracted for each ``virtual chip'', so that the remaining high 
frequency structure can be compared with that from the CFHT data. Under this procedure, the PSF 
ellipticity from both data simulations can be viewed as pure atmosphere-originated. Examples of 
these simulated residual ellipticity maps are shown in the bottom row of \Fref{fig:CFHT_maps}. Note 
that since the exposure time in the simulations is $\sim5$ times shorter, we expect the ellipticity values 
to be $\sim\sqrt{5}$ times higher
%\citep{2011arXiv1110.4913H}
-- this is manifested by the difference in 
the scales on the color bars.

%begin{figure*}
%  \begin{center}
%    \includegraphics[scale=0.4]{mos_49_e2res.png}\hspace{-0.1in}
%    \includegraphics[scale=0.4]{mos_22_e1res.png}\hspace{-0.1in} \\
%    \includegraphics[scale=0.4]{err_CFHT_52_e1res.png} \hspace{-0.1in}
%    \includegraphics[scale=0.4]{err_CFHT_34_e2res.png} \hspace{-0.1in}
%  \end{center}
%  \begin{center}
%    \includegraphics[scale=0.36]{figs/mos_9_e1res.png}\hspace{-0.1in}
%    \includegraphics[scale=0.36]{figs/mos_20_e1res.png}\hspace{-0.1in}
%    \includegraphics[scale=0.36]{figs/mos_31_e1res.png}\hspace{-0.1in}   
%    \includegraphics[scale=0.36]{figs/mos_49_e1res.png}\hspace{-0.1in}
%    \includegraphics[scale=0.36]{figs/mos_56_e1res.png}\hspace{-0.1in} \\
%    \includegraphics[scale=0.35]{figs/err_CFHTa_1_e1res.png} \hspace{-0.1in}
%    \includegraphics[scale=0.35]{figs/err_CFHT_3_e1res.png} \hspace{-0.1in}
%    \includegraphics[scale=0.35]{figs/err_CFHT_6_e1res.png} \hspace{-0.1in}
%    \includegraphics[scale=0.35]{figs/err_CFHTa_21_e1res.png} \hspace{-0.1in}
%    \includegraphics[scale=0.35]{figs/err_CFHT_29_e1res.png} \hspace{-0.1in} \end{center}
%\caption{Single-component residual ellipticity maps for two examples of the 74-second CFHT data 
%(top) and the 15-second LSST simulations (bottom). The colors indicate the residual of the ellipticity 
%values when a 2nd-order polynomial model is subtracted. The simulation are selected to have 
%visually similar features in the spatial patters. Since the exposure time is different, the absolute levels 
%of these ellipticity maps are expected to scale like $\sqrt{t}$, which is illustrated by the difference in 
%the scale of the color bars shown here.}
%\label{fig:CFHT_maps}
%\end{figure*}
%
\subsection{Comparison and discussion}

Visually, we can see from \Fref{fig:CFHT_maps} that in some of the PSF patterns predicted by the 
\phosim model, there are data that match quantitative and qualitatively. However, the \phosim models 
tend to generate a wider variation of patterns and somewhat higher ellipticity values. The wide variation 
in our simulations is expected, since they sample across random atmospheric conditions by construction, 
while the data only consists of two nights of data separated by a month. The higher ellipticity values, 
although hard to quantify, is also consistent with our understanding of the two observation sites: The 
atmospheric profiles for both sites -- Mauna Kea (CFHT) and Cerro Pachon (LSST) -- have been carefully 
studied by separate groups
%\citep[see][] {2009MNRAS.394.1121C, 2006MNRAS.365.1235T}.  
In
%\citet{2009MNRAS.394.1121C},
the authors observed that the atmospheric turbulence at Mauna Kea, 
is dominated by ground layer contributions, while this has not been observed by 
%\citet{2006MNRAS.365.1235T}
at Cerro Pachon. Since in our simulations we can only match the 
\textit{total} seeing of the data, we are effectively putting more power in the higher altitude turbulent 
atmosphere compared to the real atmospheric profile when the data was taken. This excess power in the 
higher altitude turbulent atmosphere than generates higher levels of high frequency ``ripples'' seen in 
\Fref{fig:CFHT_maps}, which cannot be taken out with the 2nd-order polynomial model -- this explains the 
higher ellipticity level in our simulations. The ripples cannot come from lower altitude atmosphere because 
the projection of the aperture on those altitudes do not span a large enough angular range as the exposure 
happens to see significant different parts of the atmosphere, which suggests that the contribution from the 
lower altitudes are usually common-mode shifts and featureless. 

The above observations can also be seen more quantitatively. We first plot the absolute values 
($\varepsilon=\sqrt{\varepsilon_{1}^{2}+\varepsilon_{2}^{2}}$) of the model-subtracted ellipticity for all the 
data and simulations in \Fref{fig:CFHT_histo}. Then, we calculate and plot in \Fref{fig:CFHT_correlation} 
the two-point spatial correlation functions for these model-subtracted ellipticity maps, defined: 
\begin{equation}
  \xi_{+}(\theta) = \langle \varepsilon_{t}(\theta_{0}) \varepsilon_{t}(\theta_{0}+\theta) \rangle + \langle 
  \varepsilon_{\times}(\theta_{0}) \varepsilon_{\times} (\theta_{0}+\theta ) \rangle \;,
  \label{eq:cf}
\end{equation}
\noindent where the subscripts $t, \times$ indicate an isotropized decomposition of 
$\boldsymbol{\varepsilon}=(\varepsilon_{1},\varepsilon_{2})$ along the line connecting a 
particular pair of galaxies. If $(\varepsilon_{1},\varepsilon_{2})$ is measured in an arbitrary Cartesian 
coordinate system, then the rotated ellipticity is calculated via 
$\varepsilon_{t}=-Re(\boldsymbol{\varepsilon} e^{-2i\varphi})$ and 
$\varepsilon_{\times}=-Im(\boldsymbol{\varepsilon} e^{-2i\varphi})$, where $\varphi$ is the argument of 
the vector connecting the pair of galaxies. The angle bracket $\langle \rangle$ indicates an average over 
all galaxy pairs separated by $\theta$ (with one galaxy located at $\theta_{0}$). 

In
%%\Fref{fig:CFHT_histo},
we show the ellipticity distribution for the simulations (solid red), the data (solid 
black), and the data projected onto a 15 second exposure time (dotted black). In \Fref{fig:CFHT_correlation}, 
we show the median correlation function for the simulations (solid red), the data (solid black), and the data 
projected onto a 15 second exposure time (dotted black). The error bars show the standard deviation (divided 
by 2 for clarity of the plot) for these curves over the ensemble.  

%\begin{figure}
%  \begin{center}
%   \includegraphics[scale=0.5]{figs/histo_CFHT.png}
% \end{center}
% \caption{Absolute model-subtracted ellipticity distribution plotted for the CFHT data (solid black) 
% and the LSST simulations (solid red). We extrapolate the CFHT data to 15-second exposure time (dotted 
% black) by multiplying the solid blue curve by $\sqrt{5}$.}
%\label{fig:CFHT_histo}
%\end{figure}
%
%\begin{figure}
%  \begin{center}
%   \includegraphics[scale=0.45]{figs/corr_compare_CFHT_paper.png}
% \end{center}
% \caption{Two-point correlation function of the residual ellipticities plotted for the CFHT data (solid black) 
% and the LSST simulations (solid red). The error bars show show the rms spread in these curves over 
% the ensemble of images (divided by 2 for clarity of the plot). We extrapolate the CFHT data to 15-second 
% exposure time (dotted black) by multiplying the solid blue curve by 5. The general shape of these curves 
% are very similar: the high frequency component in the PSF variation shows a power-law behavior out to 
% 30 arcminutes. The curve crosses zero several times and the zero-crossing scales are very similar in the 
% data and the simulations. This feature is mainly due to the 2nd-order polynomial model subtraction, which 
% creates this artifact at approximately half of the CCD chip size.}
%\label{fig:CFHT_correlation}
%\end{figure}
          
As expected from the discussion previously, the ellipticity distribution as well as the normalization of the 
correlation functions are both higher and more scattered in the simulations. In \Fref{fig:CFHT_histo}, we 
see that the difference between the median values of the model-subtracted ellipticity in the data and the 
simulations is within the scatter from the random sample of the atmospheric conditions. On the other hand, 
the correlation functions in \Fref{fig:CFHT_correlation} trace each other very well, where the power-law 
slope and the zero-crossing scales match over the entire angular scale out to 30 arcminutes (we do not 
plot the larger scales for the error bars become large and the curve becomes not quite meaningful). 
Lack of more detailed atmospheric records in the CFHT data prevents us from further comparisons. 
But the similarities between the simulations and the data shown in the comparison in this section 
gives us sufficient confident that the atmospheric model we have implemented is capturing the most 
important characteristics in terms of their effects on image distortions from the atmosphere. 




%-------------------------------------------------------------------------------\section*{Background} 

%\section{Atmospheric turbulence modelling and conventional simulations techniques}
%\label{sec:Background}

\subsection{Imaging through atmospheric turbulence}
%\label{sec:Background_theory}

The phenomenon of light propagating through the atmosphere in an imaging system has 
been well studied
%\citep{1965JOSA...55.1427F, 1966JOSA...56.1372F, 1978JOSA...68..892H,1995itt..book.....R, 1998aoat.book.....H}.
It is usually viewed as electromagnetic wave propagating 
through a random media with statistical fluctuations in the refractive index $n$ that follow the 
Kolmogorov power spectrum $\Phi_{n}^{K}$
\citep{Kolmogorov1941}:
\begin{equation}
\Phi_{n}^{K}(\kappa,z)=0.033C^{2}_{n}(z)\kappa^{-11/3}
\label{eq:kolmogorov}
\end{equation}
\noindent where $\kappa$ is the spatial frequency and $C_{n}^{2}(z)$, the structure constant 
is a function of height $z$. 

In reality, \Eref{eq:kolmogorov} cannot be true for all spatial scales $\kappa$, since the power 
in the atmosphere cannot be infinite on small $\kappa$ values. Thus, the Von Karman 
power spectrum $\Phi_{n}^{V}$
%\citep{1948PNAS...34..530V}
is more often used:
\begin{equation}
\Phi_{n}^{V}(\kappa,z)=\frac{0.033C^{2}_{n}(z)}{(\kappa^{2}+\kappa^{2}_{min})^{11/6}}
e^{-\kappa^{2}/\kappa_{max}^{2}}
\label{eq:karman}
\end{equation}
\noindent where $\kappa_{min}=2\pi/l_{out}$ and $\kappa_{max}=2\pi/l_{in}$ are associated with 
the outer scale $l_{out}$ and the inner scale $l_{in}$ respectively. 

% \chihway{[add that the energy scaling of scales]}

The inhomogeneous refractive index perturbs the phase and amplitudes of the light wave, 
causing image distortions when viewing from a ground-based telescopes. In the case where the 
amplitude variation, or scintillation is much smaller than the phase variation, or equivalently, the 
near-field condition
%\citep{1998aoat.book.....H}, 
\begin{equation}
h\leq \frac{r_{0}^{2}}{\lambda}
%\label{eq:geo_criterion}
\end{equation}
the so-called ``geometric optics limit'' is invoked and the complex calculation can be greatly simplified. 
In \Eref{eq:geo_criterion}, $h$ is the hight of the upmost turbulent layer where geometric optics is 
valid, $r_{0}$ is the Fried parameter (see X for a full definition of the Fried parameter) 
and $\lambda$ is the wavelength of the light. For typical atmospheric 
conditions at good sites, $r_{0}\approx$ 5 -- 30 cm. In the optical band of $\lambda=$ 0.3--1 $\rm \mu m$,  
\Eref{eq:geo_criterion} is essentially satisfied for the entire extent of the atmosphere, \ie the bulk part 
of the mass of the atmosphere is always safely within the region $h<$ 25 km.

To evaluate image distortions introduced by the atmosphere on a distant point source observed 
through a telescope, we can calculate the PSF at the focal plane, which is given by the square of 
the Fourier transform of the electric magnetic field at the pupil plane. Under the geometric optics 
limit, the electric magnetic field of a distant point source at the pupil can be viewed as a plane 
wave multiplied by some phase shift $\psi(\boldsymbol{\rho})$, which is determined by the 
integration of the inhomogeneous refractive index over the height of the atmosphere. We have 
%\citep{1995itt..book.....R}: 
%If we assume a long exposure 
%(see \Sref{sec:Approx_timescale}), where an ensemble average is taken over a complete sample 
%of the atmosphere, we can derive an analytic form for the circularly symmetric PSF 
 
\begin{equation}
{\rm PSF}(\boldsymbol{\theta})
=| \frac{1}{A} \int_{A} d^{2}\boldsymbol{\rho} \: e^{i k\boldsymbol{\theta} \cdot \boldsymbol{\rho} } 
e^{i \psi(\boldsymbol{\rho}) } |^{2} \; .
\label{eq:PSF}
\end{equation}
\noindent Here $\boldsymbol{\theta}=(\theta_{x}, \theta_{y})$ is the angular position on the focal 
plane, $A$ is the pupil plane, or the integration area, $k=2\pi/\lambda$ is the wave number, 
$\boldsymbol{\rho}=(\rho_{x},\rho_{y})$ is the vector connecting two points on the pupil plane 
with length $\rho=|\boldsymbol{\rho}|$, and $\psi(\boldsymbol{\rho})$ is the phase at 
$\boldsymbol{\rho}$. Writing out \Eref{eq:PSF} and changing the integration variable gives:
\begin{align}
&{\rm PSF}(\boldsymbol{\theta}) \notag \\
&=\frac{1}{A^{2}} \int_{A} d^{2}\boldsymbol{\rho}  \int_{A} d^{2}\boldsymbol{\rho'} \: 
e^{i k\boldsymbol{\theta} \cdot \boldsymbol{\rho} } e^{i \psi(\boldsymbol{\rho}) }
e^{-i k\boldsymbol{\theta} \cdot \boldsymbol{\rho'} } e^{-i \psi(\boldsymbol{\rho'}) } \notag \\
&=\frac{1}{A^{2}} \int_{A} d^{2}\boldsymbol{\rho}  \int_{A} d^{2}\boldsymbol{\rho'} \: 
e^{i k\boldsymbol{\theta} \cdot (\boldsymbol{\rho}-\boldsymbol{\rho'}) } 
e^{i (\psi(\boldsymbol{\rho})-\psi(\boldsymbol{\rho'}))} \notag \\
&=\frac{1}{A} \int_{A} d^{2}\boldsymbol{\rho''} e^{i k\boldsymbol{\theta} \cdot \boldsymbol{\rho''}} 
\left[ \frac{1}{A} \int_{A} d^{2}\boldsymbol{\rho} \: e^{i (\psi(\boldsymbol{\rho})-\psi(\boldsymbol{\rho+\rho''}))}\right]
\label{eq:PSF2}
\end{align}
\noindent In the last line we observe that the PSF is just a Fourier transform of the quantity in 
the bracket, or, 
\begin{equation}
{\rm PSF}(\boldsymbol{\theta})=FT \left[ \frac{1}{A} \int_{A} d^{2}\boldsymbol{\rho} \: e^{i (\psi(\boldsymbol{\rho})
-\psi(\boldsymbol{\rho+\rho''}))}\right]
\label{eq:PSF3}
\end{equation}
\noindent The term inside the bracket is formally known as the the Optical Transfer Function (OTF). 

\subsection{Conventional atmospheric simulations}
%\label{sec:Background_simulations}

Under the geometric optics limit, which is usually true for most astronomical imaging, a canonical way 
of modeling the atmosphere is to construct thin layers of statistically random ``atmospheric phase 
screens''
%\citep{1992WRM.....2..209L}
according to \Eref{eq:karman}, from which the phase of arriving 
electromagnetic waves are modulated as they pass through. These thin screens represent the effect of 
thick slabs of the atmosphere distributed from just above the telescope to a few tens of kilometers above. 
The screens may move in different directions to simulate the effect of wind and the turbulent structures 
on the screens are treated as static, or ``frozen'' throughout the exposure, for the time scale for these 
turbulence to change is much longer than a typical time scale for the screen to move across the aperture 
%\citep{1938RSPSA.164..476T, 2009JOSAA..26..833P}.

An instantaneous PSF is calculated by first collapsing the portions of all the phase screens that 
are in the column of air projected from the telescope aperture towards the sky and then conduct a 
Fourier transform according to \Eref{eq:PSF3}. For finite exposure time, the PSF is simply performing 
the same calculation discussed above in a large number of time steps as the phase screens move in 
each time step and add all the instantaneous PSFs together. This simulation framework has been 
implemented and tested against observational data in several AO simulation tools 
%\citep{1981PrOpt..19..281R, 2002ESOC...58..239E, 2002ESOC...58..217L, 2004SPIE.5497..290B}. 

\subsection{Practical considerations}
%\label{sec:Background_practical}

The canonical simulation scheme described above has so far been implemented on relatively small 
field-of-views. We argue below that it is largely impractical to extend this scheme to modern day 
survey telescopes if we wish to use these simulations for a wide range of purposes (\eg system 
performance evaluation, software development and science).

We consider here LSST as our benchmark survey. If we want to generate simulated images on the 
full LSST field in a reasonable time scale, we will need to simulate 10 years of data in a few months, 
with each individual exposure being a 15-second image on a 3-Giga pixel focal plane ($\sim3\times 
10^{6}$ focal plane-size images). At this scale of data volume, the first obstacle comes when we 
perform the Fourier transform in \Eref{eq:PSF3} -- this operation becomes extremely computational 
expensive for the large 2-dimensional array. The second problem occurs when we attempt to generate 
the phase screens: for each exposure, the side of an atmospheric screen needs to be larger than the 
maximum wind speed ($\sim 30$ m/s) multiplied by the exposure time ($\sim2\times15$ seconds), or 
900 m. With a typical 7-layer atmospheric model and minimum resolution of ($\sim 0.1r_{0}\sim1$ cm), 
this suggests an enormous amount of memory allocation to store the screens during the simulation 
($\sim40$ GB per screen), which is again computationally prohibited. The exact numbers for other 
surveys may vary, but the problem is obviously as we attempt to extend the conventional atmospheric 
simulation framework to larger telescopes.
    

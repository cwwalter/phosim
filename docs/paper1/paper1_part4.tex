
\section{\bf{Overview of the Method for Simulation}}

The atmosphere is modeled by a series of layers each with an independent 3D Kolmogorov model
that is averaged into an equivalent screen with refractive index variations in 2D (see Figure {~\ref{kol}}).
Each layer has an outer scale, an outer-outer scale, a random realization of Gaussian amplitude phase independent
Fourier modes with a "seeing" set by the Fried parameter (r0).
The kick vectors of the layers are normalized by the relative structure function versus height.
The time dependence of the atmospheric seeing is modeled by the frozen translating
screen for each layer (Taylor approximation) with wind velocity (2D vector set indedently for each layer).

Figure {~\ref{3screen}} shows the basic geometry of the overall context. 
Each layer has a 2D screen that captures the integrated effect
of the 3D turbulence for that portion of the atmosphere.
Each of the screens is comprised of three parts (coarse, medium and fine) that span
the dynamic range for 1 cm to 1 km that is required to capture the effect of the
turbulence and the motion of the layers driven by the wind during a modest exposure (15 seconds for LSST).
The photons emitted by stars in different parts of the field of view of the telescope
pass through different spatial portions of each layer of the turbulence air
and due to the wind velocity these portions also change as a function of time.
The consequence of this geometry is that each photon emitted by a source during a
short portion of the exposure (typically 10 ms) has a unique projected turbulence pattern.
Two stars that are close together in the sky (less than 1 arc minute) share a large portion of the
instantaneous turbulence pattern. As shown in Figure {~\ref{3screen}} the common portion
of the turbulence pattern comes from the lower layers.
The part of the pattern that is most different is from the non-overlapping projection from the
higher layers.
This basic geometry sets the spatial and temporal correlations of the shapes of the PSF.
This part of the model is necessary for the simulation of images that have the
correctly correlated integrated forms of the PSFs of stars and galaxies that are
required for the analysis of weak lensing effects.

We use actual measurements of structure functions and wind profiles to model
the layers of the atmosphere.
For LSST we use the data from the 1998 Gemini Site Testing Campaign
and from the NCEP/NCAR reanalysis archive of NOAA's Earth System Research Laboratory.
Theses profiles for the turbulence and wind are typical and show that
we require a model with large spatial dynamic range from 1 km (motion of the wind in 15 seconds)
and down to 1 cm to capture the effects of the turbulence for Fried parameters (5-30 cm)
that can be derived from the integrated structure function of the turbulence.

\section{\bf{Instantaneous PSF}}

We simulate the PSF for a point source (star)
in the context of the geometry shown in Figure {~\ref{3screen}}.
For each given instance in time we can compute the aperture projected integrated
screen that is comprised of a sum over the layers.
This instance screen (examples shown at the bottom of Figure {~\ref{3screen}} and also
on the left portion of Figure {~\ref{speckle2}})
can be used as the input to a fully diffractive analysis.
This standard approach for computing the exact diffractive PSF shows the full effects
in the right panel of Figure {~\ref{CPSF}}.
This image reveals the "speckles" that are expected from an exact calculation.
The analytic details are explained in a next section of the paper.

The light that passes throught the layers of the atmosphere that form an unwanted
time dependent optic formed by the density fluctuations enduced by the turbulent flow of the air.
These density variations are modeled by a Kolmogorov spectrum which imprints a variation in the index of refraction.
For the approximate method the
path of the photon changes direction through each layer due to the refraction that is basically the same
physical model that we use to follow the path through the lens of the camera.
We model each layer by the sum of three screens each 1024x1024 elements in 2D.
The resolution (and size) of the coarse, medium and fine screens is 1 m (1 km), 10 cm (100 m) and 1 cm (10 m) respectively.
The realization of the complete screen for each layer is a necessary practical approximation
because a single screen for each layer would require an array (and FFTs)
of a size of $10^5$ by $10^5$ or $10^{10}$ elements.
The projected aperture of a telescope captures the light from a star at a given instance
that passes through each screen that is translating according to the vector wind of that layer.
The dominant component of the refraction of the light is set by the coarse screen with a 1 m resolution that
maps the density of the air over an large aperture (for LSST $\sim8 m$).
Toridally repeated medium and fine screens have smaller effects on the final PSFs.
The details of the accuracy of the combined coarse, medium and fine screens are explained in a later section.

The effect of the wavelength of each photon and the strength of the turbulence is set by the Fried parameter(r0).
The refractive approximation requires that we apply a low pass
Fourier filter to the screen with a roll off scale of ${\sim {r0 \over 2}}$.
The detailed analytic reason for this step in the algorithm is explained in the next section.
The basic reason for this step is that the slope of a small patch of the portion of a
screen that a single photon strikes of size ${\sim {r0 \over 2}}$ is to large to be treated
as a simple refraction at a surface.
Diffractive effects become more important near a scale ${\sim r0}$.
As a final step the location of the photon is convolved with the Airy pattern for the full
aperture. This last step is optional for the context of r0 much smaller than the aperture.

Figure {~\ref{Cpix}} shows the effects of pixelization of the instantaneous PSF computed
with full diffraction. The image on the right shows the details of the many speckles with
fine details at the diffracton limit. The image on the left is the same image that has been
pixelated at a scale that removes most of the fine structure. This modest pixelization
is much finer than the Nyquest limit of the seeing disk typical of the imaging pixels
for a wide field camera for a large aperture telescope. The missing fine detail is lost
by the imaging detector. The left panel of Figure {~\ref{Cpix}} (pixelated fully diffractive image)
compares well to the left panel of Figure {~\ref{CPSF}} (approximate refractive image).
 

%Figure {~\ref{PSFspeckle}} shows simulated simultaneous short exposures of two stars separated by a
%small angle. These two PSFs are highly correlated because the projected aperture for each has significant
%common turbulence at low height and modest differences at high height above the telecope
%(see Figure {~\ref{3screen}}).
%High fidelity simulation of these details is important for weak lensing analysis
%since the correlations of PSFs (including ellipticities) of stars and galaxies are a key element of the analysis.
%Stars nearby galaxies in projection on the sky are used to calibrate the effects of the
%atmosphere on the PSF.


\section{Refractive Approximation}
\label{sec:Approx}
The first problem we raised in
\Sref{sec:Background_practical}
forces us to seek an approximate 
form to
\Eref{eq:PSF3}
that can be calculated quickly while keeping the fidelity of the important 
characteristics of image. We show in this section that a carefully implemented refractive ray-optics 
approach is capable of solving this problem.

\subsection{The intermediate time scale}
\label{sec:Approx_timescale}
Before discussing the approximation, we elaborate here on the relevant time scale of interest in this 
study. Turbulence in the atmosphere is a stochastic process, which means that both the phase 
structure function and the PSF will depend strongly on time through the variation in the random structures 
of the various atmospheric layers that cross the pupil plane at any particular instant. However, for a finite 
duration exposure, a larger footprint on these atmospheric layers is observed due to the wind, which 
means that the effective PSF is averaged over such structures. At long enough exposures where the 
sampling is nearly complete, it becomes meaningful to estimate it via statistical techniques. 
In most adaptive optics literature, it is common to talk about two distinct limits: the short exposure limit 
($\sim0.1$ seconds), where only very little averaging is happening and interference features are pronounced 
through the highly asymmetric ``speckle'' patterns in the PSF, and the long exposure limit ($\sim$ several 
minutes), where all asymmetric speckles have been averaged completely yielding a near circular Gaussian 
PSF, since there are no preferred axes in the problem. The long-exposure PSF size naturally arises from the 
average amount of de-centering of the instantaneous speckle patterns. 

Here, we are interested in the PSF size and asymmetry in a few tens-second exposures, which is in an 
intermediate regime between the short- and long- exposure limit -- the atmospheric PSF will not be round, 
but will also not have much observable speckle structures. In the following discussions, we use the single 
exposure time of LSST, 15 seconds, as the default intermediate time scale of our analyses.
%, therefore the results obtained would change if a different exposure time is of interest.  

\subsection{Scaling relations and the refractive regime}
\label{sec:Approx_scaling}
We start from revisiting the process of light propagating through one turbulent atmospheric phase screen.
Consider a two-dimensional turbulent patch on the phase screen of characteristic size $\rho$. Radiation 
can interact with that turbulent patch in two related but distinct ways: First, if there is a gradient in the 
index of refraction over the patch, it will act like a prism, so that the light will be refracted at a finite angle. 
This displaces the image of a star, \ie gives rise to image motion without broadening the image. Second, 
the finite size of the turbulent patch gives rise to diffraction. This broadens the image width, without, in 
general, causing significant displacement.

The two effects depend very differently on the size of the patch. As discussed earlier in 
\Sref{sec:Background_theory},
the power in the Kolmogorov turbulence phase structure is proportional 
to $\rho^{5/3}$. Since the image displacement due to refraction is proportion to the gradient of the phase, 
the refraction effects simply scale with $\rho^{-1/6}$:
\begin{equation}
(\delta \theta)_{\rm refraction} \approx \frac{\lambda}{2\pi} \nabla \psi(\boldsymbol{\rho}) \propto 
\frac{(\rho^{5/3})^{1/2}}{\rho}=\rho^{-1/6}
\end{equation}
\noindent where $(\delta \theta)_{\rm refraction} $ is a measure of the refractive displacement, $\lambda$ is 
the wavelength and $\psi(\rho)$ is the phase for the turbulence patch of scale $\rho$. On the other hand, the 
image broadening from diffraction, $(\delta \theta)_{\rm diffraction}$, is proportion to $\rho^{-1}$ through:
\begin{equation}
(\delta \theta)_{\rm diffraction} \approx \frac{\lambda}{2\pi \rho} \propto \rho^{-1}
\end{equation}

In addition, for large aperture telescopes, a given scale can be sampled multiple times over the pupil 
plane even in an instantaneous exposure. Thus, the averaged effect of image displacement due to that 
scale is reduced by a factor of N$^{-1/2}$, where N is the number of samplings. Since N$^{-1/2}$ is 
proportional to $\rho$, we have 
\begin{equation}
(\delta \theta)_{\rm refraction} \propto \rho^{5/6}
\end{equation}
This averaging, however, cannot be invoked for $(\delta \theta)_{\rm diffraction}$, since the diffractive 
broadening is always increasing the image width and multiple diffractions do not cancel each other 
(if not even add constructively?!). As a result, we have shown that there is a clear separation of the 
contributions from turbulent structures on large and small scales -- large scale structures contribute 
primarily to refraction, while small scale structures contribute to diffraction. 

Note that the crossover scale of the two parameters -- the scale at which diffraction has a similar 
level of effect as refraction -- is by definition the Fried parameter, $r_{0}$, formally defined via the 
integrated structure function over the line-of-sight:
\begin{equation}
r_{0}=0.185\left[ \frac{4\pi^{2}}{k^{2}\int_{0}^{L}dzC_{n}^{2}(z)}\right]^{3/5}.
\label{eq:fried}
\end{equation}

The arguments above suggests that it is possible to separate the refraction and diffraction effects 
completely by some critical spatial frequency $\kappa_{crit}=2\pi/\rho_{crit}\approx2\pi/r_{0}$, where 
the power from diffraction effects drops quickly for spatial frequencies smaller than $\kappa_{crit}$ 
and rises steeply for spatial frequencies larger than $\kappa_{crit}$. That is, we can break down the 
phase $\psi(\boldsymbol{\rho})$ as the sum of two components: 
$\psi_{<}(\boldsymbol{\rho})$, the phase shift coming from low frequency structures and 
$\psi_{>}(\boldsymbol{\rho})$, the phase shift coming from high frequency structures. 
\begin{equation}
\psi(\boldsymbol{\rho})=\psi_{<}(\boldsymbol{\rho})+\psi_{>}(\boldsymbol{\rho}).
\label{eq:breakdown_phase}
\end{equation}

% this next section has a tex problem
%Now if we reexamine
%\Eref{eq:PSF3}
%and substitute in
%\Eref{eq:breakdown_phase},
%we can write: 
%\begin{align}
%{\rm PSF}(\boldsymbol{\theta}) &\notag \\
%=FT & \left[ \frac{1}{A}  \int_{A} d^{2}\boldsymbol{\rho} \: 
%e^{i (\psi_{<}(\boldsymbol{\rho})-\psi_{<}(\boldsymbol{\rho+\rho''}))} \right \notag \\
%& \;\;\;\;\;\; \times \left e^{i (\psi_{>}(\boldsymbol{\rho})-\psi_{>}(\boldsymbol{\rho+\rho''}))}\right]
%label{eq:PSF4}
%\end{align}

At this point, we notice that the size of the turbulent structure associated with 
$\psi_{>}(\boldsymbol{\rho})$, is typically much smaller than the telescope's aperture 
$\rho<r_{0}\ll D$, where D is the aperture size of the telescope. This suggests that these smaller 
scales are very well sampled -- they effectively approach the ``long-exposure limit'' discussed in 
\Sref{sec:Approx_timescale},
even through they experience intermediate exposure times. 
As a result, the second exponential term in
\Eref{eq:PSF4} 
can be approximated by a statistical 
average that is independent of $\boldsymbol{\rho}$. The integration in
\Eref{eq:PSF4}
can then 
be written as the product of two separated integrations and the Fourier transform can also be 
separated: 

% this next section has a tex problem
%\begin{align}
%{\rm PSF}(\boldsymbol{\theta})& \notag \\
%\approx FT & \left[ \langle e^{i ( \psi_{>}(0) -\psi_{>}(\boldsymbol{\rho''}) ) } \rangle \right \notag \\
%& \times \left \left(\frac{1}{A} \int_{A} d^{2}\boldsymbol{\rho} e^{i (\psi_{<}(\boldsymbol{\rho})
%-\psi_{<}(\boldsymbol{\rho+\rho''}))}\right) \right] \notag \\
%= FT & \left[ \langle e^{i ( \psi_{>}(0) -\psi_{>}(\boldsymbol{\rho''}) ) } \rangle \right] \notag \\
%& \otimes FT \left[ \frac{1}{A} \int_{A} d^{2}\boldsymbol{\rho} e^{i (\psi_{<}(\boldsymbol{\rho})
%-\psi_{<}(\boldsymbol{\rho+\rho''}))}\right] 
%\label{eq:PSF5}
%\end{align}



\noindent where $\otimes$ indicates convolution and angle brackets $\langle \rangle$ indicates 
a full ensemble average over a random sample of these scales. The last step uses the property of 
Fourier transformations, where we have shown that the full PSF can be represented as the 
convolution of the separate PSF contributions from large and small spatial frequencies. We define 
two ``virtual'' PSF components, ${\rm PSF_{>}}(\boldsymbol{\theta})$ and 
${\rm PSF_{<}}(\boldsymbol{\theta})$ corresponding to the two Fourier transforms respectively, so 
that 

% tex problem ?
%\begin{equation}
%{\rm PSF}&(\boldsymbol{\theta}) =  {\rm PSF_{<}}(\boldsymbol{\theta}) 
%\otimes {\rm PSF_{>}}(\boldsymbol{\theta})
%\label{eq:PSF6}
%\end{equation}

The separation of turbulent scales and their PSF contributions is the key concept behind our 
atmospheric model. For $ {\rm PSF_{<}}(\boldsymbol{\theta})$, we have argued in the beginning 
of this section that since diffraction has little effects in this regime, a pure refractive ray optics 
approach is sufficient in describing the PSF centering, size and asymmetry. For 
${\rm PSF_{>}}(\boldsymbol{\theta})$, the statistically averaged diffractive effects dominate the 
PSF behaviors, leading us to a convenient way of treating the remaining part of the PSF not 
modeled by the refractive approximation. We describe the treatment for 
${\rm PSF_{>}}(\boldsymbol{\theta})$ in the next section.  




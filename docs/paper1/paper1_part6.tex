
\subsection{The long exposure limit and ${\rm PSF_{>}}(\boldsymbol{\theta})$}
\label{sec:Approx_klarge}

As hinted earlier, since ${\rm PSF_{>}}(\boldsymbol{\theta})$ is effectively in the long-exposure 
limit, where we can invoke the well known analytic form for the circularly symmetric 
long-exposure PSF (see Appendix for derivation, or reference for derivation):
\begin{align}
{\rm PSF_{>}}(\boldsymbol{\theta}) & 
\approx FT \left[ \langle e^{i ( \psi_{>}(0) -\psi_{>}(\boldsymbol{\rho''}) ) } \rangle \right] \notag \\
&= FT \left[ e^{-\frac{1}{2} D_{\psi,>}(\rho)}\right]
\label{eq:PSF6}
\end{align}
\noindent where $D_{\psi}(\rho)$ is the ``structure function'' , defined as the square of the phase 
difference of two points separated by $\rho$, averaged over all points:  
\begin{equation}
D_{\psi}(\rho)= \langle | \psi(\boldsymbol{\rho_{0}})-\psi(\boldsymbol{\rho_{0}} + 
\boldsymbol{\rho}) |^{2} \rangle_{\boldsymbol{\rho_{0}}}  
\end{equation}
\noindent The angle brackets $\langle \rangle$ indicate an ensemble average over a random 
realization of the same atmosphere power spectrum $\Phi_{n}(\kappa,z)$, $\boldsymbol{\rho_{0}}$ 
is an arbitrary point in the space considered, and $\psi$ is the phase value at each point. 
$D_{\psi}(\rho)$ can be calculated through a Fourier transform of 
$\Phi_{n}(\kappa,z)$ followed by an integrating along the line of sight in $z$: 
\begin{align}
D_{\psi}( \rho)= &8\pi^{2}k^{2}\int_{0}^{h}dzC_{n}^{2}(z) \notag \\
&\times \int_{\kappa_{min}}^{\kappa_{max}}
d\kappa \kappa(1-J_{0}(\kappa\rho))\Phi_{n}( \kappa,z),
\label{eq:integrate_phase}
\end{align}
\noindent where $J_{0}$ is the zeroth order Bessel function of the first kind. For $D_{\psi,>}(\rho)$ in 
\Eref{eq:PSF6}, since only scales smaller than $\rho_{crit}$ are considered, we have 
\begin{align}
D_{\psi,>}( \rho) = &8\pi^{2}k^{2}\int_{0}^{h}dzC_{n}^{2}(z) \notag \\
& \times \int_{\kappa_{crit}}^{\kappa_{max}} 
d\kappa \kappa(1-J_{0}(\kappa\rho))\Phi_{n}( \kappa,z) 
\label{eq:integrate_divide}
\end{align}

Substituting \Eref{eq:integrate_phase} into \Eref{eq:PSF6}
gives us a handle in evaluating 
numerically the radial profile of ${\rm PSF_{>}}(\boldsymbol{\theta}) $, which convolved with 
${\rm PSF_{<}}(\boldsymbol{\theta})$ yields the full atmosphere-induced PSF. 

\Fref{fig:OTF_breakdown} (\Fref{fig:PSF_breakdown})
shows an example of the two pieces of 
the OTF (PSF) and the combined full OTF (PSF) for $\rho_{crit}=r_{0}=0.2$ m.   

First, we note that when considering the behavior of light propagation through or around objects 
that have characteristic dimensions much larger than the light's wavelength, refractive effects 
dominate over diffractive effects and simple ray optics is a good approximation. The standard 
formalism in this limit is the ``ray equation'':
\begin{equation}
\frac{d}{ds}\left( n \frac{d\boldsymbol{T}}{ds}\right) = \nabla n
\label{eq:ray}
\end{equation}
As the object's characteristic dimension approaches the wavelength, diffraction start to dominate 
over refraction and the errors on \Eref{eq:ray} become unacceptable. For astronomical imaging 
through the atmosphere, the two phenomena yields different observable effects -- refraction 
generally introduces image motion while diffraction causes broadening of an image without 
changing its position.  

Next, consider the atmospheric model described in \Sref{sec:Background_simulations}, the 
phase fluctuates according to \Eref{eq:karman}, that is, we are given a continuous range of 
scales in this problem. The effects from the largest-scale fluctuations can be treated with pure 
ray optics but the approximation starts to break down at some critical scale where diffraction 
takes over. We can identify this critical scale $\rho_{crit}$ to be where 
$D_{\psi}(\rho_{crit})\approx2\pi$, the separation of two points on the atmospheric 
screen that have a phase difference close to one wavelength, or $2\pi$. This scale, as seen 
from \Eref{eq:structure_fried}, is very close to the Fried parameter $r_{0}$. 
\begin{align}
D_{\psi}(\rho_{crit}) = & 2\pi \approx 6.88 \left( \frac{\rho_{crit}}{r_{0}}\right)^{5/3}; \notag \\
\rho_{crit}\approx & 0.947 \: r_{0} \approx r_{0}.
\label{eq:crit_rho}  
\end{align}

% this next section causes a tex problem
%In the limit of 
%$\kappa_{min} \rightarrow 0$ and $\kappa_{max} \rightarrow \infty$ (\ie as \Eref{eq:karman} 
%approaches \Eref{eq:kolmogorov}) and substituting the first integration with the definition of 
%Fried parameter (\Eref{eq:fried}) gives the well known expression from \citet{}:
%\begin{equation}
%D_{\psi}(\rho) \approx 6.88 \left( \frac{\rho}{r_{0}}\right)^{5/3},
%\label{eq:structure_fried}  
%\end{equation}

However, there are significant variations in the image motion for moderate exposures, which are in 
fact the principal causes of residual anisotropies, so these must be modeled correctly.  That means that 
the short exposure limit is also inappropriate

The latter is appropriate, because as we show below, the primary effect of the larger scales 
for short exposures is image displacement, not an increase in PSF width.

This is done 
by realizing that for scales smaller that $\rho_{crit}$, they are effectively always in the long-exposure 
limit, for they are sampled a large number of times in LSST's large-aperture even in the intermediate 
time scale. (work out numbers?)
This suggests us to revisit \Eref{eq:integrate_phase} and separate the second integration 
into two integration ranges from $\rho_{crit}$, this isolates the part of the integration that cannot be 
approximated with ray optics accurately. First we write out the structure function into the sum of two 
individual structure functions:
\noindent The $<$ ($>$) subscript specifies that the range of frequency in the integration is below 
(above) $\kappa_{crit}$, where $\kappa_{crit}=2\pi/\rho_{crit}$. Since $D_{\psi}( \rho)$ appears in 
the exponent in \Eref{eq:PSF} we can also define the two PSF components corresponding to the 
two structure functions:
\begin{align}
\rm{PSF_{LE}}(\boldsymbol{\theta})
=&FT \left[ e^{-\frac{1}{2} D_{\psi,<}(\rho) }e^{-\frac{1}{2} D_{\psi,>}(\rho) }\right] \notag \\
=&FT \left[ e^{-\frac{1}{2} D_{\psi,<}(\rho) } \right]  \otimes FT\left[ e^{-\frac{1}{2} D_{\psi,>}(\rho) } \right] 
\end{align}
\noindent The first term is the PSF coming from phase fluctuations on scales larger than $\rho_{crit}$, 
while the second term comes from scales smaller than $\rho_{crit}$. We will refer to the two terms as  
$\rm{PSF_{LE,<}}$ and $\rm{PSF_{LE,>}}$ respectively. The complete long-exposure PSF is the 
convolution of these two components. As discussed above, for the intermediate exposure time in 
our case, we can use $\rm{PSF_{LE,>}}$ to represent the piece of the PSF that is not simulated 
properly using ray optics. 




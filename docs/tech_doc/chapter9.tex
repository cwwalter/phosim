\chapter{Validation of Photon Propagation in the Atmosphere}
\chaptermark{Atmosphere $\gamma$ Propagation Validation}

\section[Task 1A:  Test of the Refractive Approximation]{Task 1A:  Test of the
  Refractive Approximation \sectionmark{Task 1A}}
\sectionmark{Task 1A}

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{figure_speckle.eps}
\end{center}
\caption{Sample PSF with speckles. The top row shows the separate effects of the coarse, medium anf fine screens.
The bottom shows the similar PSF from a nearby star. The two PSFs share part of the path through the air as
defined by the partly overlapping projected aperture of LSST.\label{speckle}}
\end{figure}

\begin{figure}[ht!]

\begin{center}
\includegraphics[width=5.5in]{figure_task1a_star.eps}
\end{center}
\caption{A instance screen at the scale of the LSST annular aperature with the associated speckled image.\label{speckle2}}
\end{figure}

\begin{figure}[ht!]

\begin{center}
\includegraphics[width=5.5in]{figure_task1a_samples.eps}
\end{center}
\caption{Comparison of real and simulated images.\label{speckle3}}
\end{figure}

\noindent
{\it Requirement:  Show that important diffraction
effects are includes in the photon path algorithm (screen functions for each
layer and the raytrace code). The validation requires a direct compare
son of simulated PSF with the geometric raytrace and full diffraction.}
\vspace{0.5\baselineskip}

The purpose of this task is to show that each simulated PSF for a point source (star)
is in a form of speckles that are expected from a full calculation based on diffraction.
The light that passes throught the layers of the atmosphere that form an unwanted
time dependent optic formed by the density fluctuations enduced by the turbulent flow of the air.
These density variations are modeled by a Kolmogorov spectrum which imprints a variation in the index of refraction.
The path of the photon changes direction through each layer due to the refraction that is basically the same
physical model that we use to follow the path through the lens of the camera.
We model each layer by the sum of three screens each 1024x1024 elements in 2D. The resolution (and size)
of the coarse, medium and fine screens is 1 m (1 km), 10 cm (100 m) and 1 cm (10 m) respectively.
The projected aperture of the LSST captures the light from a star at a given instance
that passes through each screen that is translating according to the vector wind of that layer.
The dominant component of the refraction of the light is set by the coarse screen with a 1 m resolution that
maps the density of the air over the $\sim$8 m aperture of the LSST.
Figure ~\ref{speckle} shows an example of the image of a star (top left) with the full screen,
the same star (top middle) with the fine screen removed, and finally the same star (top right) with only the coarse screen.
As expected the number and placement of the speckles is mostly set by the coarse screen with some shifts
in the speckles as the effects of the medium and fine screen are included.
The lower three images are computed for the same screens for a second nearby star that shares a portion of the screen with the first star.
However the geometric path through the screens is shifted enough to change the speckle pattern for the second star.
Clearly the speckle pattern is highly correlated as expected from two stars separated by a small angle.
The simulation ensures that
the full nonlinear PSF is computed for each instance in time during a 15 s exposure for each separate point within
the full LSST field of view. Overall the correlations in the spatial and temporal structure of the PSF are accurately
represented. The wavelength dependence of the speckle pattern is reproduced by a special Fourier filter that is applied
to the screen to set the scale of the pattern of the speckles. This spatial filter is a function of the Fried parameter approximated
for the average wavelength of the particular LSST filter passband. This model produces a realistic speckle pattern
but does not capture the detailed diffraction effects of the Airy pattern that each speckle would show at high sub-pixel
resolution. This limitation is not an issue for an LSST simulation since the optics of the telescope and the camera are
not diffraction limited. This approach works well for a wide field large aperture telescope (degrees across the field)
but would not be appropriate for an AO system that seeks diffraction limited performance over a narrow field of view (less than $\sim$1 arc minute).

Figure ~\ref{speckle2} an example of an instance screen (sum of the coarse, medium and fine parts)
and the corresponding speckle pattern.
We show the full screen at the scale of the LSST aperture (left panel) and compute the associated PSF (right panel).
The left part of Figure ~\ref{speckle3} shows two examples of actual speckle patterns from existing telescopes.
The right part of the figure shows two similar examples of speckle patterns for two examples of simulated stars.


\section[Task 1B: Test of Screen Convergence]{Task 1B:  Test of Screen
  Convergence \sectionmark{Task 1B}}
\sectionmark{Task 1B}

\noindent
{\it Requirement:  Perform a convergence test for screens that shows that
  physics is independent of grid resolution}
\vspace{0.5\baselineskip}

The atmospheric model captures the physics of the turbulence by implementing a
series of numerical screens.  The numerical screens are rather complex in that
there are multiple layers with multiple grid resolutions.  The use of the
screens by the raytrace involves determining the kick vector depending on
where each photon hit at a given time.  The final positions of the photons,
and therefore the size, shape, and centroid of the PSF is affected by these
photon kicks.  Therefore, it is important to test even if the atmospheric
model is incorrect, if the positions of the photon are independent of the grid resolution.

Below we simulated a single star with an identical set of parameters for 9
separate simulations.  For each simulation, we
measured the ellipticity, centroid, and size (5 parameters).  The only
variation was that the turbulence screens were used in nine different ways.
First, the medium \& fine screens were ignored in the calculation completely, and only every fourth pixel
was used in the coarse screen.  In this way, we were crudely representing the
atmospheric screens by a very coarse screen that was undersampled.  The second simulation was
identical except we used every second pixel in the coarse screen.  The third simulation simply used all
pixels on the coarse screen.  The fourth simulation continued using all pixels
on the coarse screen, but added every fourth pixel on the medium size
screen.  This continued until we used all pixels on all three screens for the ninth
simulation.  Below we show the effective resolution (by calculated the
effective pixel size) and the five measurements of the star compared to the
value when we used all pixels.  The data indicated that the size, shape, and
position of the star have approximately converged after adding the last
fine screen.  We compare the last to second last point of each of the five
measurements to calculate the screen convergence.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation1.ps}
\end{center}
\caption{Validation of the convergence of the turbulence screens for task 1B.}
\end{figure}

\section[Task 1C:  Test of Atmospheric PSF Effects]{Task 1C:  Test of
  Atmospheric PSF Effects \sectionmark{Task 1C}}
\sectionmark{Task 1C}

\noindent
{\it Requirement:  Validate the multi-layer model of the atmosphere with wind
  speeds and structure function of the turbulence and demonstration that the
  PSF including form and correlations across the field of view are present at
  the correct levels. }
\vspace{0.5\baselineskip}

The atmosphere model creates a different PSF for every point in the field (as
well as a different one depending on the SED of the source).  It does this
because the time averaged set of photons will result in a different set of
displacements as they pass through the series of turbulence screens drifted at
different velocities.  If the screens has a very fine scale pattern (say a
coherence scale of 1 cm), then the resulting PSFs would be perfectly round
because the angular displacements would average out with more and more
photons.  However, with the real atmosphere the turbulence pattern has a much
larger scale to the pattern (several meters), so over reasonably short
integration times, the PSF has a finite and measureable ellipticity.  Below,
we first test whether the atmospheric seeing is what is intended, and then we
compare the ellipticity distribution to Subaru data.  The spatial correlation
scale is then compared.

To validate the atmospheric PSF, we simulated grids of stars across the full
focalplane (one on each raft, and one on each chip for the central 5 by 5
chips).  We decreased the pixel size to 1 microns, turned off all optics
perturbations, and increased the brightness to 18th magnitude so their were
no statistical or pixel effects on the results.  We repeated this simulation
for 5 different realizations of the atmosphere with 1 arcsecond seeing.  We
then measured the PSF size and ellipticity for all the stars.  The figures
below show those measurements.  The upper left plot shows that the measured
sizes approximately agree with the input to the simulator (1'').  There is
some variation at the few percent level across the field.  The upper right
plot shows the ellipticity distribution of stars.  The ellipticity is produced
by the fact that the turbulence screens produce an incomplete averaging of the
isotropy of the PSF.  We compared this to measurements of the ellipticity of
PSFs using 15 Subaru exposures.  Since Subaru has a similar aperture size to
LSST, and the exposures were the same as the LSST exposure time, this is
probably the most appropriate data to make a comparison.  The bottom plots show the 2
point correlation of both the PSF size and the ellipticity.  The fact that the
size is highly correlated is not surprising because of the frozen-screen
approximation.  The ellipticity correlation has a angle around 1 degree.  This
is roughly expected because the ratio of the typical outer scale to the height
of the atmospheric turbulence sets this angular scale, since stars larger than
this angle will experience a completely different turbulence pattern.  We have
compared both of these correlations to the same Subaru data.  It has a similar
fall off in the ellipticity correlation and a flat profile in the size
correlation.  In our model, the exact correlation varies from one atmosphere
to the next, so with only the current data we can it roughly agrees given the uncertainties.
We do
not yet have data that validates the correlation on large scales, because of the inability to
have a telescope with a large enough field of view, similar aperture size, and
have the ability to take 15 second exposures.  We calculate three quantities
from these measurements:   1) the number of standard deviations the average PSF
size is from 1.0, 2) the number of standard deviations the mean ellipticity is
from the Subaru mean ellipticities given their width, and 3) whether the
log of the ellipticity correlation angle is within 1 dex of 1 degree.  The sum
of these three statistics gives our validation value.


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation2.ps}
\end{center}
\caption{Validation of the Atmospheric PSF for Task 1C.}
\end{figure}


\section[Task 1D:  Test of Atmospheric Astrometric Effects]{Task 1D:  Test of Atmospheric Astrometric Effects \sectionmark{Task 1D}} 
\sectionmark{Task 1D}

\noindent
{\it Requirement:  Validate the variation in the
differential location of stars across the full field of view between pairs of
15 second images including the correlations of these different vectors. }
\vspace{0.5\baselineskip}

The atmosphere induces small astrometric motions.  The exact position of a
star on the focal plane will shift even if all optics perform perfectly.
Furthermore, the atmospheric shifts are predicted to vary across the field, so
the guiders in the corner raft cannot remove this effect.  There will also be
some small shift in the positions of stars between the two exposures.  This
validation test determined if the astrometric shifts are reasonable.  

Tim Axelrod obtained some calibration data from the LBT telescope where a
crowded star field was deliberately trailed across the CCD during the
exposure.  The stars motion in the dimension transverse to the trailing then
accurately measures the astrometric motions of the atmosphere in
one-dimension.  In general, he found that stars a few arcminutes from each
other had highly correlated trails (also predicted by the atmospheric model),
so we did not have a large enough field to accurately measure the correlation
scale.  On the other hand, we can accurately compare the astrometric jitter
due to the atmosphere.  Axelrod measured the mean 1-D shift of the atmospheric
astrometric jitter in 15 second segments and produced the red dashed histogram
in the bottom plot.  We then simulated the a series of trails with the same
trailing speed for 10 atmospheres.  The atmospheric jitter is seen in the top
plot.  We also produced the average shift in 15 second intervals, and compared
with the LBT data in the bottom plot.  We then calculate the KS p-value
probability of these two distributions coming from the same parent
distribution.  We do not expect perfect agreement since the results will vary
from one value to the next, but it appears consistent.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{lbt.ps}
\end{center}
\caption{1-D centroid motion of the LBT data showing the atmospheric
  astrometry variation.  The pixel size is 0.224 arcseconds.  This motion
  compares favorably with the simulations in Task 1D.}
\end{figure}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation3.ps}
\end{center}
\caption{Validation of the Atmospheric Astrometry for Task 1D.}
\end{figure}


\section[Task 1E:  Atmospheric Description]{Task 1E:  Atmospheric Description \sectionmark{Task 1E}}
\sectionmark{Task 1E}

\noindent
{\it Describe in Detail the Physical Models for: 1) the turbulence in the
  dome, 2) the model and variation of cloud opacity, 3) the
  multi-layer/multi-component model (including wind shift) of atmospheric
  transmission (including water, molecular oxygen, ozone, and Rayleigh
  scattering) with cross-sections and density variation as a function of
  altitude, 4) the wavelength dependence of the seeing, 5) the model for wind
  speed and direction as a function of altitude and its annual variation
  appropriate for the LSST site, 6) the model for turbulence intensity as a
  function of altitude appropriate for the LSST site, 7) the model for the
  outer Kolmogorov scale as a function of altitude as well as the maximum of
  the Kolmogorov scale (the outer-outer scale), 8) the model for the total
  atmospheric seeing and its annual variation, 9) the model for atmospheric
  dispersion and its wavelength dependence}
\vspace{0.5\baselineskip}

The description of the atmosphere and more detailed other atmospheric physical effects are
found in Chapter 4.

\chapter{Photon Sampling from Astrophysical Sources}

\section{Properties of the Photon}

We first create photons from astrophysical sources.  The photons have several
properties that are dictated by astrophysical models of the source:  the
direction, the wavelength, and the arrival time.  The total number
of photons are calculated from the magnitude normalization (described below).
Finally, the photons evenly sample the entrance aperture projected to the top
of the atmosphere which sets the initial three dimensional position.

\subsection{Direction}

The direction of the photon is computed using a spatial model for the
source which defines a relative position of each photon as an offset from the right ascension
and declination of the source. Internally, the direction is always converted to a
three-vector relative to the coordinate system.  The spatial model has parameters
that model can also be the effect of gravitational lensing.

\subsection{Wavelength}

The wavelength of the photon is determined by sampling from the SED files (see
below).  The SED files are linearly interpolated between grid points.  The
wavelength of the photon is redshifted after it is sampled from the SED.

\subsection{Time}

The arrival time of a photon is uniformly chosen between 0 and the maximum
exposure time.  The absolute time offset relative to the beginning of the exposure pair is
used in some time-dependent calculations.

\subsection{Position}

The initial position of the photon in 3-D is calculated in the following way.
First, the entrance aperture (an annular shape) is sampled uniformly in x and
y.  Second, we look ahead to the shape of the primary mirror to calculate a z
position given the values of x and y.  Third, we then use the direction of the
photon to move the ray backwards to a height of 20 km.  This results in a new
x and y position.  This results in photons sampling the entrance
aperture as efficiently as possible, but is approximately equivalent to
sampling photons filling the projected annular aperture at the top of the atmosphere ($\sim$20 km)

 an annulus at 20 km shifted by the angular offset.  

\subsection{Numbers of Photons}

The total number
of photons for a particular source are calculated by considering the AB magnitude at a particular
wavelength (in our case, 500 nm divided by (1+redshift)) and
converting that a flux in ergs per sq. centimeter per second per Hz.  Then,
the Spectral Energy Distribution (SED) is converted to a relative fraction of
photons in each bin.  Using the probability of finding a photon in the bin near the
reference wavelength (500 nm/(1+redshift)), one can then calculate the total
number of photons per sq. centimeter per second from that source at all wavelengths.
Then the total number of photons are calculated by multiplying by the aperture
size and exposure time.

Photons are randomly sampled from all the sources in proportion to their
relative photon fluxes, until the total number of photons for all sources
(with a Poisson error added) is simulated.

\section{Astrophysical Models}

\subsection{Star \& Asteroid Models}

Our spatial model of a star is simply a point.  The asteroid model specifies
a velocity vector, so the spatial position will vary given the current arrival
time for that photon.  

\subsection{Galaxy Models}

\noindent
{\it Reference: \cite{lorenz}, \cite{sersic1963}}
\vspace{0.5\baselineskip}

The galaxy model normally used specifies an ellipsoidal sersic distribution in
two dimensions.  The sersic index, major and minor axis, and position angle
are the parameters.  Then a single galaxy is normally constructed out of two
sersic models:  one representing the bulge and another representing the disk.
Each has its own sersic index.

\subsection{SED sampling}

The SED files are sampled by constructing cumulative distributions, and
drawing photons according to that probability distribution.  The wavelengths
are linearly interpolated between bin points.  The photons are
first absorbed by the first dust model using this wavelength, then the photons
wavelengths are redshifted, and then they are absorbed by the second dust
model at the new wavelength.    

\section{Dust Models}

\noindent
{\it References:  \cite{ccm1989}; \cite{calzetti2000}}
\vspace{0.5\baselineskip}

There are two dust models given by the Cardelli, Clayton, \& Mathis (1989) model and
the Calzetti et al. (2000) model.  Each model has parameters:  $A_V$ and
$R_v$.  The models are stored in a variety of grid lookup tables and the dust
is applied by first calculating the optical depth given the photon
wavelength.  Then we destroy photons with probability $e^{-\tau}$.
Performing dust extinction through a Monte Carlo approach conveniently avoids
construction of a unique SED for every single source.

\chapter{Non-Astronomical Source Simulation}

Following, we describe how we generate background photons, diffuse dome flat
photons, and cosmic ray events.

\section{Moon \& Sky Background}

\noindent
{\it Reference:  \cite{krisciunas1991}; \cite{patat2006}; \cite{bankert};
  Gemini spectra}
\vspace{0.5\baselineskip}

We use a background model where we model the background out of two components:  the dark
sky and the moon.  The dark sky's is assumed isotropic on the sky and has a
wavelength dependence given by the SED described below.  The dark sky's
brightness is assumed to be 22.09 AB magnitudes at 5000 angstroms plus a
variation related to the variation in water vapor we used in the opacity
model (18\%).  The assumption is that all of the dark sky background is
due to water lines, which is mostly true.  The water vapor variation is then
squared, because the emission is proportional to density squared and this is
converted to the actual magnitude per sq. arcseconds.  The mean sky background
and its variation are consistent with that found by Krisciunas \& Shaefer (1991).
The sky brightness is increased near twilight according to the sun's altitude
using a color-dependent model of Patat et al. (2006).

The moon's intrinsic brightness as a function of its phase and altitude
follows the calculation of Krisciunas \& Shaefer.  We use the lunar spectrum below
to represent the wavelength dependence.  We then need to predict the
brightness where the telescope is pointing.  Here we use the Krisciunas \&
Shaefer formula which has a term for the Rayleigh scattering of the moonlight
as well as the Mie scattering of the moonlight.  Krisciunas \& Shaefer were
only calculated the lunar brightness for one band so we simply scale the
Rayleigh term by inverse wavelength to the fourth power.  Mie scattering is
wavelength independent.  

We then can make a prediction for the sky brightness as a function wavelength for
every pixel in the image.  The background brightness varies across the field
because of the moonlight.   We then need to predict the actual counts in the
image, where we need to describe the calculation in the next section.  We also
note that we also a trivial extension of this model is that we remove the
background when the shutter is closed to simulate darks.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{background_sed.ps}
\end{center}
\caption{The spectral energy distribution of the background components:  the
  dark sky and the moon.  The SEDs are taken from Gemini data.}
\end{figure}


\section{Count Rate Prediction}

\noindent
{\it Reference:  \cite{bankert}}
\vspace{0.5\baselineskip}

Simulating the actual moon and sky background, one photon at a time would
increase the simulation time by a factor of at least 30.  Therefore, we
developed a fast technique to calculate the count rate in every pixel approximately.
To predict the actual count rate in each pixel, we pre-run the simulator using
scaling the sky brightness and SEDs used for the two background components.
This involves first simply recording the conversion formulae for each filter
given the SEDs used for each component.  Then when a new sky brightness is
considered the zeropoints are already known exactly,  we do not have to run
the simulation for every background exactly.  The one major telescope effect
that must be included is the vignetting as a function of field angle.  To do
this, we now simulate patches of a flat illuminating sources as a function of
field angle and record it in a numerical table.  The background program then
accurately predicts the count rate in every pixel.  This is then distorted by
the Poisson counting error.

\section{Dome Light}

A flat that would be produced from a dome light illumination pattern actually
uses the exact same calculation described for the background.  Therefore, to
simulate flat we use the same program, but instead use the dome light SED
(currently assumed to be flat) and the dome light illuminating profile
(currently assumed to have a perfectly isotropic pattern).  We can use this
same infrastructure to simulate monochromatic flats.

\section{Cosmic Rays}

\noindent
{\it Reference:  \cite{gilmore}; \cite{doty}}
\vspace{0.5\baselineskip}

Real images of cosmic rays are added to the simulated images using real data from
thick devices.  We first have constructed postage stamp images of 130 different
actual cosmic ray events.  We then add these randomly to our simulated images
using two important calculations.  To determine how often to place a
cosmic ray in the image, we use the production rate of 0.04 cosmic rays per
sq. centimeter of Silicon per second.  This produces about 1 cosmic ray per
amplifier per 15 second exposure. The actual cosmic rays are expected to
be a combination of gamma rays from local ground radiation and muons and other
particles from atmospheric particle interactions.  Our data has some
combination of the two, but it may not be in correct proportions.  A second
calculation gives us the scaling of electrons in the cosmic ray data, to the
appropriate volume of silicon in the LSST pixels.  This correctly normalizes
the intensity of the cosmic ray images (i.e. makes the correct number of ionized
electrons in our devices).

\chapter{Electron Readout Physics}

Below we describe the physics the electron readout.  This mostly involves
taken the image from the previous step and adding camera defects or readout
complications with some physics models.

\section{Amplifier Segmentation, Readout Orientation, Pre \& Over Scans}

\noindent
{\it Reference:  \cite{document7822}}
\vspace{0.5\baselineskip}

The amplifier segmentation and readout direction are taken from the Rasmussen
data in document-7822 \& 7821.  The implementation of the segmentation is
straightforward, because the pixels can simply be copied from the physical
piece of silicon in the raytrace to new image files representing the
individual amplifier data.  The readout direction is important for both in
arranging the pixels in readout order in the amplifier images as well as
determining the direction of charge transfer inefficiency calculations.  It is
also used for the hot column algorithm.

The pre-scan (virtual pixels read out earlier) and over-scan (virtualpixels read out later) regions are added to the edges of the image to
represent the virtual pixels normally represented in the digitization process
or pixels unexposed to photons.  Currently, the pre-scan pixels in the serial
direction add a buffer of 4 pixels, and the pre-scan pixels in the parallel
direction adds 1 pixel.  There are currently no over-scan pixels.  The number
of pre-scan pixels is currently insufficient for a robust offset calculation.

\section{Relative QE Variation}

\noindent
{\it Reference:  \cite{gilmore2}}
\vspace{0.5\baselineskip}

For the pixel to pixel variation of the Quantum efficiency, we used a simple
model where the QE was given in each pixel as 1 minus a single-sided gaussian with mean 0
and a $\sigma$ of 1\%.  We considered having a correlation length in the
variations, but found that the correlation length was not much more than 1
pixel in real flats.  The QE variation is fixed throughout all simulations,
and currently has no time-dependence.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{qevar.ps}
\end{center}
\caption{An example of the quantum efficiency variation on the individual
  pixel level for various regions.}
\end{figure}

\section{Read noise and dark current}

\noindent
{\it Reference:  \cite{gilmore3}}
\vspace{0.5\baselineskip}

Read noise is implemented by adding a gaussian with mean of 2 electrons to the
value in each pixel (but a negative value is not added).  Read noise is added to the pre/overscan pixels as well.
There is a 3\% variation between amplifiers in the read noise value that is
fixed throughout all simulations.  Dark current is added at the rate of 2
electrons per second of exposure.  The total number of electrons added per
pixel is given a Poisson error.  There is no amplifier dark current variation
between amplifiers or between exposures.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|}
\hline
Parameter & Value & Variation \\ \hline \hline
Read Noise & 2 $e^{-1}$ &  3\% \\ \hline
Dark Current & 2 $e^{-1}/s$ & 0\% \\ \hline
\end{tabular}
\end{center}
}
\caption{The read noise and dark current model parameters.}
\end{table}


\section{Hot pixels, Hot Columns, \& Dead Pixels}

\noindent
{\it Reference:  \cite{gilmore4}; \cite{ahmad}}
\vspace{0.5\baselineskip}

Hot pixels are added to the image in the same location throughout all
exposures.  Hot pixels are always set to the full well depth with no
variation.  The rate of hot pixels is set to 0.0125\%, which is somewhat high
compared to Calypso measurements (see Z. Ahmad note).  Similarly, dead pixels
defined as pixels with 0\% quantum efficiency are included at the rate of
0.0125\% and do not vary between exposures.

The total number of pixels in the full camera that are assigned to hot columns
is 0.025\%.  This is accomplished by first randomly deciding some fraction of
hot pixels that will produce a hot column and then also marking the pixels
after that pixel in the readout process as hot.  There is no variation in hot
columns between exposures.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|}
\hline
Parameter & Value & Variation \\ \hline \hline
Hot Pixels & 0.0125\% &  0\% \\ \hline
Dead Pixels & 0.0125\% & 0\% \\ \hline
Hot Columns & 0.025\% & 0\% \\ \hline
\end{tabular}
\end{center}
}
\caption{The hot pixel \& column and dead pixel rates.}
\end{table}

\section{Gain, Bias, Non-Linearity, Charge Transfer Inefficiency}

\noindent
{\it Reference:  \cite{gilmore5}; \cite{ahmad}}
\vspace{0.5\baselineskip}

After every pixel in the image has its complete number of $e^{-1}$ determined
from all sources, the conversion of the the image from electrons into
analog-to-digital units (ADU) that would occur by a real analog-to-digital
converted (ADC) is a straight-forward calculation.  We use the formula below

$$ ADU = \frac{e p s}{G \left(1-N \frac{e}{W} \right)} + B$$

\noindent
where $e$ is the number of electrons, $G$ is the gain, $W$ is the full well
depth, $B$ is the bias, $N$ is the non-linearity factory, $p$ is the
non-linearity in the par.  The gains are
set to 1.7 with a 3\% variation between amplifiers.  Note a value of 1.7 gets
the maximum well depth of 100,000 electrons below the 16-bit limit of 65,536.
The amplifier biases are set to 1000 ADU with a 2\% variation.  The
non-linearity factor is currently set to 0 with no variation.  We have studied
the variation of bias as a function of column to build a more realistic model,
but this is not currently implemented (see Z. Ahmad note).  The charge
transfer inefficiency (CTE) factors calculate the number of pixels from the readout
in both the parallel and series direction, and then scale the number of counts
by the number of pixels from the readout raised to the power of 0.999555 for
the parallel CTE and 1.0 for the serial CTE.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|}
\hline
Parameter & Value & Variation \\ \hline \hline
Bias & 1000 ADU &  2\% \\ \hline
Gain & 1.7 $e^{-1}$/ADU & 3\% \\ \hline
Non-linearity & 0.0 & 0\% \\ \hline
Serial CTE & 1.0 & 0\% \\ \hline
Parallel CTE & 0.999995 & 0\% \\ \hline
\end{tabular}
\end{center}
}
\caption{The electron count rate to ADU conversion model.}
\end{table}

\section{ADC errors}
 
The digitization process can produce errors it the conversion from electrons
to ADU.  We implemented a digitization sequence simulation by taking the
predicted ADU in every pixel from the previous step and calculating the value
in the $i$th bit by 

$$ {\mbox bit_i}=\left( \frac{ADU}{2^i}  +{\mbox ADCERR_i} \right)~~{\mbox mod}~~2$$

\noindent
and then the final ADU value is given by

$$ADU = \sum_i bit_i 2^i$$

\noindent
where the $ADCERR_i$ values keep the digitization from being perfect.
Currently, however we have set all those errors to 0.



 

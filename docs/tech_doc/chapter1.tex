\chapter{Requirements, Scope \& Overall Approach}

\section{Scope}

The {\it photon simulator} is a novel set of codes that accomplish the task of 
taking the description of what astronomical objects are in the sky at a
particular time ({\it the instance catalog}) as well as
the description of the observing configuration ({\it the operational
parameters}), and then produce a realistic data stream of images that are similar to
what a real telescope would produce.  To understand the approach used in the
photon simulator, we first discuss the dual implicit requirements that have driven its
software development.
The {\it photon simulator} was also developed with some approximations.
For example, we developed the simulator for  large aperture
wide field optical telescopes, such as the planned design of LSST.
This class of telescopes are not diffraction limited in contrast to narrow field, large aperture telescope
that are typical of adaptive optics system.
The initial version of the simulator also targeted the LSST telescope and camera
design.  This model has now been broadened to include existing telescopes of a related nature.
The atmospheric model, in particular, includes physical approximations that are limited
to this general context.

\section{Requirements}

We have been working to meet essentially two overall requirements about how we
decide to build and improve the simulator.
\begin{itemize} 
\item{{\bf Fidelity Requirement:}  {\it The physics of the simulator is required to
have sufficient fidelity to be able to reproduce the photometric,
astrometric, and image quality (point-spread-function size and shape) properties of a
17th magnitude astrophysical object in a ten year stack.  The physics is required to include the full wavelength-dependent
and time-dependent physics, so that the image quality are accurate for
possible variations in the spectral energy distribution (SED) of the source
and for the simulated exposure time.}  A 17th magnitude source produces about 1
million photons for a typical LSST exposure and would be partially saturated,
so this is a reasonable brightness to require complete fidelity.  Brighter
objects should still be accurately rendered, but increasingly obscure physical
effects that would not be observable in a 17th magnitude object would be not
formally required.  A stronger form of this requirement would be to accurately
produce images for a 17th magnitude object in all exposures over the 10 years
lifetime of LSST, as well as reproduce the sky background properties as well
for the measurements of faint properties.  This would result in about 1000 times more photons for that
object, and a much stricter set of requirements.  The astrometric, photometric, and PSF size/shape properties can be mapped to
image quality accuracy tolerances that can be seen below the shot noise limit for a 17th magnitude
object.  However, it is clear that we are not at sub-percent accuracy in
either the design of LSST itself or many of the physics models.  This will
probably be effectively accomplished after all the improvements described in
part III are made.  It can reasonably argued, however, that we are achieving
complete fidelity (effects that can be seen above the shot noise) for the
faintest objects (24th mag) in typical images already.  These fainter objects
are where a large fraction of the science will be done.
 In general, if you can measure a physical effect in an
image for objects where actual measurements will be made, the simulator should
eventually include that effect.}

\item{{\bf Speed Requirement:}   {\it The speed of the overall simulation framework should be
  sufficient to be able produce images at a significant fraction of real-time
  LSST production rates (700 uncompressed Megabytes per second)
  using all available computational resources of the image simulation group.}  A large fraction of the science goals of LSST involve complex image
  processing including stacking or differencing multiple 3-gigapixel images
  obtained over a several year baseline.  This implies that in order to test
  algorithmic performance on many science topics sufficiently, a few sets of full stacks of
  LSST images obtained with all filters would be required.  Thus, a reasonable
  estimate for the typical size of an algorithmically useful test is
  approximately $\approx 6$ fields $\times$ 6 filters $\times$ 100 visits $\times$ 36 seconds per exposure $\approx$ a few nights
    of data.  Thus, to make reasonable progress in understanding LSST
  algorithms and simulations and to have many software development cycles, a few nights of LSST data should be able to be
  simulated in at least a few week period.  A reasonable long term goal, although not a
  requirement, would be to actually produce data at actual real-time rates as the Data
  Management software system would likely not plan on being able to process the data
  at far higher rates than the actual real time production rate.
  For data challenges to date we have acheived a simulation rate of order $\sim$50 visits per day
  as compared to $\sim$700 visits for each night of LSST operations.
  The focal plane of LSST is comprised of 21 rafts each with a 3x3 arrays of
  4k x 4k CCDs.
  To date using thousands of processor cores we have approximately matched the real time image rate of a single LSST raft.
  }

\end{itemize}

Thus, we argue that we have not met either requirement in its strongest form,
but we are on a path to approaching these goals through continued software
development, physics and design modeling, and additional validation.

\section{Efficient Photon Monte Carlo on Grid Computing}

In order to meet the dual requirements above, the photons simulator has used
the novel approach of a sophisticated set of {\it numerically efficient photon Monte
  Carlo} codes.  The codes have then be deployed for use on large-scale {\it grid computing} platforms.  A photon
Monte Carlo is the mechanism to produce an image in the following way:

$$ {\mbox{Catalogs~~}}_{\overrightarrow{\mbox{photon~sampling}}} {\mbox{~~Photons~~}}_{\overrightarrow{\mbox{atmosphere, telescope, \& camera physics}}} \mbox{~~Images}$$

\noindent

The photon Monte Carlo approach involves first creating photons from the objects
in listed in the input catalog that we designate the "instance catalog" since it defines
the celestial sources that are present in the LSST field of view for a pair of 15s exposures
that is the basic minimum quantized period of time for LSST operations or an effective "instance".
Then, we follow those photons through the atmosphere,
telescope, and camera using wavelength and time-dependent atmosphere and
instrument physics.  The photon may convert into a photoelectron in
the Silicon of a CCD, and the electrons are then collected into pixels.
Finally, the readout
physics is simulated to produce a highly accurate image.  The final product is
then the counts of electrons in each pixel.   A photon Monte Carlo
uses the general approach where the position and trajectory of a photons at a
given time and wavelength are tracked.  Often complex wavelength-dependent physics can then
be accomplished with a single (or often few lines of code).  The photon Monte
Carlo approach enables us meet the fidelity requirement.  We expect we can
continue to improve the simulator further by having more sophisticated
physical descriptions of where and how the photon can interact in its path.
A Monte Carlo approach is the most efficient method for simulating the
vast majority of primary science targets (galaxies or stars near the detection threshold).
Signal celestial objects that are readily detected in single single chip scale images
or in 10 years stacks of images that are as weak as a few tens of photons in a single 15s exposure.
Modern sophisticated computational techniques are used throughout the photon
simulator to efficiently produce images of these priority objects.  

Grid computing is a method of dividing the computational process across
thousands of processors.  This is particularly useful for our problem because
an individual processor can work on some fraction of the photons.  The most
practical division we have found is to have one processor work mostly the
photons landing on an individual LSST CCD.  The architecture of the code, is
somewhat more complicated than this, however, as we describe in the following section.  In general, efficient code that
works on an individual workstation will also work efficiently on grid
computing.  However, keeping the memory per core below 1 to 2 Gbytes and the
data I/O below few gigabyte per CPU hour is rather important.
These limitations would not normally be constraints when developing
code for a single desktop computer.  The combination of modern efficient
numerical techniques as well as grid computing make the second requirement achievable.

At the current time we have met most (but certainly not all) of
the fidelity requirement by representing the photon physics in great detail.  Similarly, in the most recent data challenges, we
have produced data at 5$\%$ of actual real-time LSST production rates, which
has allowed us to produce millions of images using millions of CPU hours on a
variety of grid computing platforms.  The dual requirements are sometimes
contradictory in that the code could have higher fidelity but would be slower
(i.e. a fully diffractive EM code calculation), or could have lower fidelity
but would be faster (i.e. a simple gaussian PSF convolver).

In the plot below, we show the number of photons emitted from stars and
galaxies in the Universe.  As the red dotted line shows, most of the photons
detected from a telescope come from stars.  However, stars brighter than 
~17th magnitude will saturate in a typical LSST exposure, making the accurate
simulation of these photons less important.  The photons from the objects in
the 17th to 28th magnitude range, however, are critical.  Since LSST has an
effective aperture of 34 square meters and an exposure lasts 15 s, the black
dashed curve can be multiplied by these numbers to see that the vast majority
of objects will have either hundreds or thousands (but not millions) of
photons detected from them.  This is the ideal regime for a Monte Carlo.  Each
photon can be accurately simulated, but since there are only hundreds per
object it is computationally efficient.  Following, we show some examples of
results achieved by the photon Monte Carlo approach.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{speed1.ps}
\end{center}
\caption{The photons emitted from the stars and galaxies in the Universe.  The
dotted lines show the total numbers of photons and the solid line shows a
typical estimate for the numbers of stars and galaxies.  The dashed line shows
the number of photons per object.  These distributions vary across the sky,
and the number of photons (given between 0.3 and 1.2 microns) will depend on
the SED of the source, but it will be within a factor $\sim$2 of the calculation shown in the plot.}
\end{figure}

\section{Sample Results}

In the following figures, we show some examples of images where photons have
been traced through the atmosphere, telescope, and camera.  Figure 1.2 and 1.3
shows a detailed amplifier image with stars and galaxies.  Currently, we have
already simulated more than 10 million of these amplifer images.  This is a comparable
amount of data to that produced in the lifetime of many existing telescopes.
It is also obviously more data than could ever be studied by a single human.
Figure 1.4 shows the simulation of an entire LSST focal plane with its 3
gigapixels that cover 10 sq. degrees.  This simulation takes about 500 CPU
hours, and is now routine on our grid computing platforms.  Figure 1.5 shows
sample PSFs when we turn on successively more physical effects.  The wide
range of physical effects that will ultimately limit LSST's photometric
astrometric, and PSF performance are visible.  A system that can generate a
large amount of data with high fidelity details can then be used to accurately
determine whether science goals can be achieved as well as improve algorithmic
performance.

The use of simulated images by the data management group is extensive.  There
have been four data challenges where simulated images have been used (DC3a,
DC3b PT1.0, DC3b PT1.1, and DC3b PT1.2) over the last few years.  We cannot
even come close to describing the various ways the simulations have been used
in this document and this is described elsewhere in data management reports.
However, it is useful to look at the top line statistics of the last data run.
In the table below, we list the statistics calculated from the pipeQA results
demonstrating the full effectiveness of the combination of the simulation and
data management software.  Each statistic is a mixture of:  1) the current quality of the
  data management software, 1) the current realism of the photon simulator and
  astrophysical catalogs, 3) the survey design strategy, and 4) the design of the
  telescope and camera.  This table shows the power of the simulations in that
the statistics are impossible to know with real data, but they easily can be
constructed with simulations by comparing with the truth.  This also
demonstrates the importance of high fidelity in the simulations, so that we
are not misrepresenting realistic data for the data managment group.  The fact
that the results are somewhat reasonable at this stage in the project for the
huge volume of simulated data is an important milestone.

\begin{table}[ht!]
\begin{center}
{\tiny
\begin{tabular}{|l|l|l|}
\hline
Metric   & Value & Affected by \\ \hline \hline
{\bf Data Volume} & 449 visits; 2.7 million amplifier images & Scope of PT1.2 \\ \hline
{\bf Completeness}  & 23.45 magnitudes (180 million  & DM source detection algorithms;
Realism of photon simulator background/seeing; \\
(Magnitude of 50\% Completeness) & detected
sources) & OpSim survey design strategy; \\
& & Realism of magnitude distribution in astronomical catalogs \\ \hline
{\bf Photometry}  & 15 millimags (1.5\%) & DM Photometric
Algorithms; \\
(Systematic Photometric Error & & Realism of physics that causes photometric errors; \\
Compared to Truth) &  & (atmosphere opacity, clouds , instrumental chromatic effects, vignetting,etc.); \\ 
& &  SEDs of Astronomical Objects \\ \hline
{\bf Astrometry}  & 40 milliarcseconds & DM WCS and centroiding algorithms ; \\
(Median Astrometric Error  & (0.22 arcseconds WCS accuracy) & Realism of astrometric physics (atmosphere,  \\
Compared to Truth) & & optical perturbations and misalignments, tracking) ; \\
 &  & Design of Telescope and Camera ; \\
& &  Completeness because of photon statistics \\ \hline
{\bf PSF}  & 0.90 arcseconds & DM PSF Characterization Algorithm;
Realism of atmospheric refraction physics, \\ 
(Mean PSF Width Measurement) &  & Accuracy of optics misalignment physics
\& CCD Charge Diffusion; \\
& & Design of Telescope and Camera ; Survey seeing conditions \\ \hline
{\bf PSF}  & typically 0.0 to 0.15 & DM PSF Characterization Algorithm;
Realism of atmospheric refraction physics, \\
(Ellipticity Measurement) &  &  Accuracy of optics misalignment physics
\& CCD Charge Diffusion; \\
& & Design of Telescope and Camera ; Completeness because of photon statistics\\ \hline
\end{tabular}
}
\end{center}
\caption{\label{tab:dmresults}  The high level results of the entire
  PT1.2 simulation run that was used in data management pipelines.  Neither
  the image simulation nor the data management pipelines are fully developed at this
  point. Nevertheless the production of these measurements is a major milestone. }
\end{table}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{amp0_3.ps}
\includegraphics[width=5.5in]{amp0_2.ps}
\includegraphics[width=5.5in]{amp0_1.ps}
\end{center}
\caption{An example of three adjacent amplifiers.  The amplifiers are 509 by
  2000 pixels, and cover a region on the sky of about 1.7 by 6.7 arcminutes.
The bright star that crosses the amplifier boundary shows diffraction spikes
due to the secondary support spider and charge bleeding since many CCD pixels
exceed a full well of charge.}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{amp1_3.ps}
\includegraphics[width=5.5in]{amp1_2.ps}
\includegraphics[width=5.5in]{amp1_1.ps}
\end{center}
\caption{Another example of three adjacent amplifiers.  The amplifiers are 509 by
  2000 pixels, and cover a region on the sky of about 1.7 by 6.7 arcminutes. }
\end{figure}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{fp_small.ps}
\end{center}
\caption{A simulation of an entire focal plane for LSST.  The 193 different
  CCDs are represented by the squares, and the rectangular individual amplifiers (16 per chip) are also
  visible.  These can be compared to the size of the previous plot to see the
  enormous detail that LSST collects in a single exposure.  At this resolution,
  only the hot columns, and an occasional bright star can be seen.
  The vignetting of the light at the outer edge of the eight chips that are most distance from the
  center of the focal plane is clearly visible.
}
\end{figure}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{twelve_psf.ps}
\end{center}
\caption{Example point-spread-functions with successively more physics
  included.  The rgb colors represent the u-r-y filters, showing the
  wavelength dependence of some physical affects.  The details of the physics
  can be rather important in determining the number of photons at each
  wavelength (photometry), the average position of a source (astrometry), and
  the objects image quality (PSF size and shape).}
\end{figure}



\section{Future Plans}

Arguably, the photon simulator is not even close to being in a completed
form.  Since the photon simulator is a particularly novel approach in optical
astronomy, the most efficient coding and self-consistent physics models are
still being discovered by the ImSim team.  In the Figure below, a rough set of metrics demonstrate that the code
development is continuing at a rapid pace and is, in fact, accelerating.
Therefore, what appears in the code now and in this document is a
{\it snapshot} in time.  However, it is a convenient time to describe the current
configuration, obtain formal validation approval from the project, and discuss
the future development priorities.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{simhistory.ps}
\end{center}
\caption{Some imperfect metrics that show the production and code development
  of the photon simulation and catalog constructor codes.  These plot
  demonstrate that the codes are still in rapid
  development.   The first few years were mostly spent developing the
  raytrace, and the increase in the last few years includes the infrastructure
to simulate multiple observations (photon simulator non-raytrace codes), the
catalog codes, the validation scripts, and grid computing infrastructure.}
\end{figure}

There are many planned future refinements in the
physics and the efficiency that come in a
variety of types:  coding bugs, changes to the LSST design, internal
consistency of multiple parts of the code, computational efficiency, grid
computing restructuring, generalization to other telescopes, improved
interfaces, and better physics models. In the Part II (the physics
description) and Part IV (the validation tests) of this document, however,
we restrict our discussion to what is in the current code at the moment.  We
describe a detailed plan for the future in part III of this document.  In the following
chapter describe the scope of what we call the photon simulator or {\it PhoSim}.
 As we describe in the
following chapters, the photon simulator codes have considerable complexity.
For the most part, we have followed a coding design approach where we attempt to meet our
requirements and represent the physics as self-consistently as possible.  

\section{Additional Detailed Documentation}

This work attempts to document the physics in considerable detail, but in many
cases we will refer to the specific internal documentation about a given
topic.  The internal imSim documents are available at http://lsstdev.ncsa.uiuc.edu/trac/wiki/LSSTImageSim



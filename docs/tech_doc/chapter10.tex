\chapter{Validation of Photon Propagation in the Telescope \& Camera}
\chaptermark{Telescope \& Camera $\gamma$ Propagation Validation}

\section[Task 2A: Test of Raytrace Accuracy and Astrometric Scale]{Task 2A:
  Test of Raytrace Accuracy and Astrometric Scale \sectionmark{Task 2A}}
\sectionmark{Task 2A}


\noindent
{\it Requirement: Perform simulations to test the accuracy of the raytrace using a detailed spot diagram for off-axis pointings for the ideal positions of the optical elements.  The focal plane position of the spot diagram tests the astrometric accuracy of the simulator.  This will simultaneously test the reflection and refraction calculations, the wavelength-dependent index of refraction model, and the implementation of the correct optical prescription. }
\vspace{0.5\baselineskip}

The accuracy of the entire raytrace system is best tested by a spot diagram.
A spot diagram tests the ray intercept calculations, the ray reflection and
refraction calculation, and the implementation of the optical design.  Constructing a spot diagram involves tracing the rays, but turning off many components of the physics
including the perturbations, charge diffusion, large angle scattering, and
atmospheric physics.  In this way, the final positions of the rays will have a
complex pattern due to the details of the ideal optical design. 

We compare the distribution of photon positions
with that of optical engineering codes, such as ZEMAX, which are
well-studied.  The positions of photons using ZEMAX were provided by Ming
Liang (NOAO).  To do this comparison, we simulated a single star at 1.41 degrees off-axis (the center of raft 42).
The resulting spot diagram has a highly complicated pattern that is
wavelength-dependent, so we chose 5 wavelengths:  0.54, 0.58, 0.62,0.66, and
0.70 microns.  This pattern is impossible to reproduce if there is any
inaccuracy in the raytracing calculation or there is any mistake in the
implementation of the optical design.  Note this also tests the PSF shape from
the perfect optics, as well as the astrometric scale since we also compare the
absolute position in the focal plane.  The figure below compares our
calculation with that calculated from ZEMAX with the current optics model.
For each spot diagram, we calculate the overlap of the rays in the ImSim
positions with that of the ZEMAX rays.  The photons are binned on the same
scale (0.08 microns) and the overlap fraction is calculated on bins where we
have at least ten rays.  The raytrace is probably accurate on scales much
smaller than this, but would require even more photons to test, and the
accuracy is clearly already below 1\% of a pixel.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation4.ps}
\end{center}
\caption{Validation of the Raytrace accuracy for Task 2A.}
\end{figure}

\section[Task 2B:  Test of Perturbations of the Optical System on the PSF Size
  and Shape]{Task 2B:  Test of Perturbations of the Optical System on the PSF
  Size and Shape \sectionmark{Task 2B}}
\sectionmark{Task 2B}

\noindent
{\it Requirement: Perform simulations to demonstrate that the perturbation on
  the optical elements including misalignments of mirrors, lenses, filter,
  detector and surface deformations on the mirrors, lenses, filters, and
  detectors agree with the PSF budget.}
\vspace{0.5\baselineskip}

There are various components of the model that have a significant effect on
the actual PSF without atmospheric effects.  Although, the details divided
into the tolerances for every single imperfection of the telescope and camera
(as in the engineering flowdown documents, Document-3535), we can roughly divide the
components on the non-atmospheric PSF into four parts:  the aberations from
the intrinsic optical design, the sum of all perturbations and misalignments
of all parts of the optical system, the charge diffusion in the silicon, and
the internal seeing inside the dome and camera.  In this task, we will test
the tolerances with the output of the photon simulator for these four parts,
and leave the validation at a great level of detail until the design matures
and the physics models improve.   The engineering flowdown lists the maximum
allowable tolerance for these components at:  0.097, 0.259, 0.246, and 0.090
arcseconds, respectively.  The assumption here is that if the telescope and
camera is built properly then the simulator should also meet these
requirements.  

First, we have tested a variety of realizations of the optics perturbations and then
measured the PSF size with the atmosphere, internal seeing, and charge diffusion turned off.  In
general, the PSF from the optic design itself plus the perturbations has a
very complex looking PSF.  It will be important to keep this PSF contribution
to also keep the PSF ellipticity low.  This is shown in the first two rows in
the Figure below.  In the bottom row, we show the effect of the tracking, the
designed (aberrated) PSF, and the internal seeing.  Note that all of these PSF
have the effect of the optical aberrations, since that cannot be turned off.
The effect of charge diffusion is measured in Task 3B.  We then measure the
size using the formula in the SRD,

$$\mbox{FWHM}=  0.663 \sqrt{\frac{\left( \sum_i I_i \right)^2}{\sum I_i^2}} $$

\noindent
where $I_i$ is the intensity of the $i$th pixel.  The measured sizes are 
within tolerances in the engineering flowdown requirements (design 0.097'',
internal dome seeing 0.090'', perturbations and misalignments 0.259'', and
charge diffusion 0.246'').  Thus, our optics PSF components are set so they are reasonably close
to the tolerance, but do not have a significant chance of exceeding that tolerance.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation5.ps}
\end{center}
\caption{Validation of the perturbations for Task 2B.}
\end{figure}

\section[Task 2C:  Test of Throughput of Optical Elements]{Task 2C:  Test of
  Throughput of Optical Elements \sectionmark{Task 2C}}
\sectionmark{Task 2C}

\noindent
{\it Requirement: Demonstrate the throughput versus wavelength of the entire optical system is correct.  This includes testing the mirror efficiency, the lens coatings, the detector A/R coatings, the filters, and the QE. }
\vspace{0.5\baselineskip}

The throughput of the simulation involves making sure that the correction
efficiency as a function of wavelength agrees with the nominal expectation.
This insures an accurate photometric simulation.  In the photon Monte Carlo
approach, this simply involves counting photons at each stage of the
simulation and making sure the correct amount were accepted and rejected at
each stage of the process.  In some cases, the agreement should be perfect
since we applied a simple transmission curve (e.g. with the mirrors).  In
other cases, we have a more complex model (e.g. the multi-layer
angle-dependence or the mean free path of the photon in Silicon) and it is our
goal to match the throughput curve (in document-1777) by adjusting the physics
parameters.  In the following two sets of figures, we compare the throughput
after each stage of the simulation:  atmosphere, mirrors, L1+L2, Filter, L3,
Detector, and total throughputs.  The black line of document-1777 should be
compared with the on-axis, airmass=1, no clouds or opacity variation
simulation shown in the red curve.  We also show a higher airmass and a larger
off-axis angle (blue and green curves) to show how the throughput can vary
across the field and from one exposure to the next.  The only visible
discrepancy occurs with comparison with the QE curve at wavelengths longer
than 1.1 microns.  This occurs because the mean free path in silicon at 1.1
microns in the physics model is much larger than the thickness of the device,
so we cannot change any parameters to get those photons to convert.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation6.ps}
\end{center}
\caption{Validation of the system throughput for Task 2C.}
\end{figure}
\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation7.ps}
\end{center}
\caption{Validation of the system throughput for Task 2C.}
\end{figure}

\section[Task 2D:  Test of Monte Carlo Diffraction Model]{Task 2D:  Test of
  Monte Carlo Diffraction Model \sectionmark{Task 2D}}
\sectionmark{Task 2D}

\noindent
{\it Requirement:  Model for Diffraction pattern due to the multi-layer spider
  support structure. Validation requires a comparison of the fast single
  photon ray trace method with the Fourier transform of the aperture.}
\vspace{0.5\baselineskip}

We can compare the diffraction from the Monte Carlo edge diffraction model we
described previously, with the exact calculation from diffraction theory.  In
the Figure below, we demonstrate that the basic pattern is reproduce in the
Airy envelope and the spike pattern.  On the left, we did a simulation of a star
with a flat SED with diffraction turned on, but with the charge diffusion, optical
perturbations, and atmosphere turned off.  The simulation is done on-axis with
no rotation of the spider.  The right plot shows an elaborate
calculation doing the full Fraunhofer integral where we integrate over the
spider design.  We chose to perform the integral averaging the results at
wavelengths of 0.57, 0.62, and 0.67 
microns.  The calculation takes several days.  The power in the core (less
than 50 microns), spikes (within 25 microns of the pattern and not in the core)
disk (all other regions) are similar.  We construct our validation statistic
by computing the rms difference between the intensity in these three
components. 


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.0in]{validation8.ps}
\end{center}
\caption{Validation of the diffraction model for Task 2D.}
\end{figure}


\section[Task 2E:  Description of Telescope \& Camera Models]{Task 2E:
  Description of Telescope \& Camera Models \sectionmark{Task 2E}}
\sectionmark{Task 2E}

\noindent
{\it Requirement: Describe in Detail the Physical Models for: 1) the control
  system and the effect on perturbation/misalignment parameters including the
  values defined by physics and those that are under active control, 2)
  describe the dependence of the model on elevation effects (gravity), thermal
  effects and the performance variation in time (sunrise to sunset), 3) the
  focal plane layout model, 4) the detector misalignment model, 5) the use of
  OpSim inputs and their application in ImSim, 6) the time-dependent tracking
  model including rotation tracking, 7) the model for bore site pointing,   field rotation, and focus control, 5) L1, L2 and L3 model of fixed surface
  variation due to fabrication effects, 8) the 3D model for the
  pre-compensation design L3 which includes pressure gradient and thermal
  gradient effects on the index of refraction and the resulting photon path 9)
  models of filters including off-axis effects, 10) the guider model: tracking
  performance with and without guiding, 11) vibration effects, temperature and gravity gradient effects on camera optics,  12) surface pitting, volume effects (bubbles) - glass quality, index of refraction variations in camera optics, 13) the model for the shutter – variation in performance (sub-frame time dependence), 14)  special 3D model for the pre-compensation design L1,  15) the thickness and flatness map, 16) the mechanical control of chip within rafts (gaps, function of elevation (gravity) and thermal drift), 17) the model the photo-response non-uniformity, 18) the large angle (incoherent) scattering model, 19) coherent light scattering (ghosts), 20) effects that rotate during an exposure (spider, atmospheric dispersion)
}
\vspace{0.5\baselineskip}

The description of the telescope and camera and more detailed other telescope
and camera physical effects are
found in Chapter 5.

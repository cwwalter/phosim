\chapter{Code Architecture \& Interfaces}

In this chapter, we describe the basic structure of the photon simulator ({\it
  PhoSim}) codes, and then
discuss the input and output of the photon simulator.  This then defines the
scope of the photon simulator.

\section{Code Architecture}

We have designed a set of 10 novel C++ codes that optimally represent the
physics self-consistently to describe the detection of a photon with a
telescope looking through the atmosphere.  In Figure~\ref{fig:arch}, we show the
overall architecture including the relationship to the operational simulator
(OpSim) and the catalog simulations.  The granularity of various codes are organized to parallelize the larger
computational bottlenecks of the code.  The raytrace, which is the bulk of the
computation, simulates a single chip at a time on a given processor.  This was
a compromise between the simulator becoming I/O dominated if smaller regions
were simulated, and to make the unit of computation as small as possible.
We are I/O intensive at the beginning (inputs) and the end (outputs) of the
simulation of a single chip scale image.
We are computationally dominated during the phase when we track the path of every
photon through the system (2 to 3 hours with one core per chip). 

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{arch.ps}
\end{center}
\caption{\label{fig:arch} The basic architecture of the image simulation framework.  The blue
  boxes show the 10 set of codes which comprise the photon simulation codes.
  Both the operation simulator (red) and the instance catalog constructor
  (purple) are the input to the simulator.  The entire process results in the
  simulation of images.  The dotted lines show the granularity of the parallelization.}
\end{figure}

All code is now entirely written in C/C++, Python, or Shell scripting
languages.  It has been on the NCSA code subversion (SVN) repository since 2008, but developed since 2004.
All revisions have been tracked through the SVN repository, so the revision
history of both the code as well as the LSST-specific data files can be
followed.   There have been more than 2000 code revisions on the approximately 20,000 lines of code.  The codes have
been written by members of the ImSim group.  A variety of simulation ideas and design
parameters have been adapted from members of the entire LSST consortium.

The performance and structure of the code on grid computing is not fully described
here.  Table~\ref{tab:codes}, however, lists of each of
the codes and roughly points to the reasons for the current code
architecture.  The simulation time for a complete visit depends on the number
of point-like and diffuse sources at a given magnitude, $m$

% $$ 30~\mbox{minutes}+ \int dm~~ \left( 2.5^{16-14}\mbox{asinh}  \left( 2.5^{14-m} \right) N_{\mbox{point}} (m)  + 2.5^{16-m} N_{\mbox{diffuse}} (m) \right)~\mbox{minutes}$$

% try to make three lines


\vspace{0.5cm}
{\bf Fixed Overhead:    }     $$ 30~\mbox{minutes}$$ 
\vspace{0.5cm}

{\bf Stars:             } $$ + \int dm~~ \left( 2.5^{16-14}\mbox{asinh}  \left( 2.5^{14-m} \right) N_{\mbox{point}} (m) \right)~\mbox{minutes}$$ 
\vspace{0.5cm}

{\bf Extended Sources: } $$ + \int dm~~ \left( 2.5^{16-m} N_{\mbox{diffuse}} (m) \right)~\mbox{minutes}$$ 
\vspace{0.5cm}

\noindent
where 16th is about the magnitude where it can simulate a star in one minute,
and 14th is the magnitude where saturation effects become dominant.  The
code optimizations cause saturated objects to be simulated faster than for one
photon at a time (faster than linear) for point-like objects.  Therefore, the
simulation speed has a rather complex form.  The computational time of the
codes that are not simulating photons (i.e. not the raytrace) are also a
significant fraction of the computational time (fixed overhead for a chip scale image).

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{speed2.ps}
\end{center}
\caption{Measurement of the speed of the components of the photon simulator.
  This demonstrates that we have found a way to simulate the bright saturated stars more
rapidly, so we spend most of the computational time simulating the vast
numbers of stars and galaxies that LSST will measure near the detection
threshold.  The catalog construction time, and the I/O time are not included,
but are significant.}
\end{figure}

Our purpose in this document is to describe the physics embodied in these codes
rather than literally document the purpose of each code.  In part II, we have
organized the physics into Chapter 3 through 7, and these roughly follow the
code organization.  Chapters 3 through 5 describe the physics in the raytrace
for the sky, atmosphere, and telescope/camera models.  In addition, the
atmospheric parameter, atmospheric screen, cloud screen, tracking parameter,
and optics parameter codes have physics contained in these three chapters.
Chapter 6 describes the simulation of non-astronomical sources as in the
background and cosmic ray codes.  Finally, chapter 7 describes the readout
physics embodied in the e2adc code.  The following sections first describe the input
and output data of the photon simulator, and therefore define the scope of the
photon simulator.

\begin{table}[ht!]
\begin{center}
{\tiny
\begin{tabular}{|l|l|r|r|r|r|r|r|}
\hline
Code & Primary & Lines &  CPU  & Parallelization & Wall  &
Fraction of & I/O \\
  & Author & of code & Time & Factor & Time & Wall Time & \\ \hline \hline
Instance Catalog Generator & Connolly, Krughoff,     & 10000& 15 minutes & field & 15 minutes &
10-15\% & 3 Gbytes\\
 & Pizagno, Gibson & & & & & &  \\ \hline 


Photon Simulator Controlling Scripts & Peterson, Grace &  500  & negligible & field &
negligible & 0\% & negligible  \\ \hline
Atmospheric Parameter Generator & Young &  500  & negligible & field & negligible & 0\%
& negligible\\ \hline
Atmospheric Screen Generator    & Jernigan & 1000 & 1 minute   & field & 1 minute & 1\% &
1 Gbyte \\ \hline
Cloud Screen Generator          & Jernigan & 500  & 1 minute    & field & 1 minute & 1\%
& negligible \\ \hline
Tracking Parameter Generator    & Peterson & 100  & negligible & field & negligible &
0\% & negligible\\ \hline
Optics Parameter Generator      & Todd & 500  & negligible  & field & negligible &
0\% & negligible\\ \hline
Trim Program                    & Meert, Bankert & 500 & 60 minutes & 21 rafts
pairs& 1
minute & 1\% & 3 Gbytes\\ \hline
Raytrace                        & Peterson & 10000 & 500 hours  & 378 chips & 90 minutes &
60-70\% & 10 Gbytes\\ \hline
Background                      & Bankert & 1500 & 3 hours    & 378 chips & 1 minute &
1\% & 10 Gbytes \\ \hline
Cosmic Rays                     & Young & 500  & 2 hours    & 378 chips & 1 minutes &
1\% & 10 Gbytes \\ \hline
$e^{-}$ to ADC                  & Peterson & 1000  & 4 hours     & 6048 amps & 1
minute & 1\% & 10 Gbytes\\ \hline
Total I/O Latency     &    &      &  & & 15-30 minutes? &
10-20\% &  \\ \hline \hline
CCD flatness (global)         & Rasmussen & 1000  &   & all fields &

 &  & 1 Gbytes\\ \hline 
Input to Background \& Det defects (global) & Peterson, Bankert & 1000  &    & all fields & 
 &  & 1 Gbytes\\ \hline
Validation Pipeline (global) & Peterson, Todd & 5000  &    & all fields & 
 &  & 1 Gbytes\\ \hline 
\end{tabular}
}
\end{center}
\caption{\label{tab:codes} A list of the various codes of the photon simulator and their
  computational efficiency \& parallelization. Global scripts are not run on
  grid computing. }
\end{table}





\section{Input}

The basic input of the photon simulator consists of three parts:

\begin{itemize}

\item{{\bf Observing configuration parameters:} These are parameters defining the pointing,
  position of moon and sun, etc. that are typically taken from OpSim
  simulations.  The intention is to not use absolute time in the photon simulator, and so the details about when the observation is
  taken, precession, etc. are left outside the simulator.  These parameters tend
  to be similar to the types of controls a telescope operator would specify,
  and parameters affected by the time of observation. }

\item{{\bf Instance Catalog}:  A list of all astronomical objects that are
  within a {\it 2.1 degree radius circle} (which covers the full focal plane) at the
  particular time of the {\it exposure pair}.  There are many parameters that can
  describe the object that we list below.  The catalog normally consists of
  parameterized models, but could also include lists of postage stamp
  truth images.}

\item{{\bf Spectral Energy Distribution (SED) files}:  A reference set of SED files to be referenced in the
  instance catalog.  Since the number of objects in a typical instance catalog
  can be around 10 million, there cannot be a different SED for every object.
  Similarly, the redshifting of photons have to be done in the simulator (to
  not make a unique SED for every galaxy), so
  these are the rest frame SEDs.}

\end{itemize}

\subsection{Observing Configuration Parameters}

The 21 parameters that specify the observing configuration are given below.
These are used by some of the 10 codes in the photon simulator to parameterize
the physics used to simulate the images.

\begin{table}[ht!]
\begin{center}
{\tiny
\begin{tabular}{|l|l|l|}
\hline
Type &  Parameter & Notes \\ \hline \hline
Telescope & Unrefracted\_RA\_deg & OpSim derived right ascension converted to \\ 
                   &                      & unrefracted Value in units of decimal degrees\\ 
                   & Unrefracted\_Dec\_deg & OpSim derived declination converted to \\
                   &                      & unrefracted Value in units of decimal degrees\\ 
                   & Unrefracted\_Azimuth & OpSim azimuthal angle converted to
decimal degrees \\ 
                   & Unrefracted\_Altitude & OpSim altitude angle converted to
decimal degrees \\ 
                   & Opsim\_rotskypos & OpSim RotSkyPos (angle of sky relative \\
 & & to camera) in decimal degrees\\ 
                   & Opsim\_rottelpos & OpSim RotTelPos (angle of spider \\
& & relative to camera) in decimal degrees\\ 
& Opsim\_filter & Filter configuration \\ 
& SIM\_TELCONFIG & (0=normal, 1=dome/shutter closed, \\
 & &  2=dome closed/dome light
on)  \\ \hline
Moon & Opsim\_moonra& OpSim moon right ascension in decimal degrees\\ 
& Opsim\_moondec &OpSim moon declination in decimal degrees \\ 
& Opsim\_moonalt & OpSim moon altitude in decimal degrees \\ 
& Opsim\_dist2moon & OpSim distance to the moon in decimal degrees \\ 
& Opsim\_moonphase & OpSim moon phase (0=new,100=full) \\ \hline
Sun & Opsim\_sunalt & OpSim sun altitude in decimal degrees \\ \hline
Time       & Opsim\_ObsHistID & Labels files for a particular visit \\ 
           & Opsim\_expmjd & International Atomic Time (Informational Only)
\\ 
           & Slalib\_date & Date calculated from TAI \\
           & SIM\_VISTIME & Total time of the exposure visit \\ \hline
Atmosphere & OpSim\_rawseeing & FWHM in arseconds of atmospheric turbulence \\
           & & at zenith at 500 nm \\ \hline
Photon Simulator Specific & SIM\_SEED & Random Seed \\ 
                          & SIM\_MINSOURCE & Minimum number of sources to \\
& & perform simulation on a chip (Often 0 or 1) \\ \hline
\end{tabular}
}
\end{center}
\caption{Operational parameters that are necessary for the photons simulator.}
\end{table}

\subsection{Instance Catalog Format}


The catalog of objects should be a gzipped ASCII
file and have the following form.  The parameters will be read in at single
precision, if that is found necessary for any of these parameters.  Each line
should contain one model, but a single astronomical objects may be composed of
multiple models.

\vspace{0.5cm}
\noindent
{\bf
   object~~ID\#~~ra~~dec~~mag~~SED\_filename~~redshift~~$\gamma_1$~~$\gamma_2$~~$\mu$~~$\Delta ra$~~$\Delta dec$~~spatial\_model\_name
 
 spatial\_par\_1~~spatial\_par\_2~~...~~rest\_frame\_dust\_model~~dust\_par\_1~~dust\_par\_2
 
 lab\_frame\_dust\_model~~dust\_par\_1~~dust\_par\_2}

\noindent
An entry in the catalog is actually a command
to the Monte Carlo as well, which begins with the word object.  We define each entry as follows:

\begin{itemize}

\item{ {\bf object:} The first entry of each line that also acts as a command for the raytrace.} 

\item{ {\bf ID\#:}  A floating point number that is unique identifier of the object (no internal 
use by the photon Monte Carlo).}

\item{{\bf ra:}  The right ascension of the center of the object or image in decimal
  radians.}

\item{{\bf dec:} The declination of the center of the object in decimal radians in
  J2000 coordinates.}

\item{{\bf mag:}  The normalization of the flux of the object.  The units of
  the flux internal to the Monte Carlo are $\mbox{photons} \mbox{cm}^{-2} \mbox{s}^{-1}$, but we use AB
  magnitudes at 5000 \AA /(1+z) (which is roughly equivalent to V (AB) or g (AB)).}

\item{{\bf SED\_file:}  The name of the SED file relative to the data directory.}


\item{{\bf redshift:}  The redshift (or blueshift) of the object.  Given that redshift is
used quite often, it does not make sense to redshift the SED file externally.
The wavelength of the photons are shifted by (1+$z$) within the photon Monte
Carlo.  Any cosmological dimming must be included in the flux normalization.}

\item{ {\bf $\gamma_1$:}  The value of the shear parameter $\gamma_1$ used in weak lensing.}

\item{ {\bf $\gamma_2$:}  The value of the shear parameter $\gamma_2$ used in weak lensing.}

\item{ {\bf $\mu$:}  The value of the magnification parameter given in magnitudes.}

\item{ {\bf $\Delta \alpha$:}  The value of the declination offset in radians.  This
  can be used either for weak lensing or objects that move relative to another
  exposure if you do not want to change the source position in the first two columns.
  $\Delta \alpha$ is a small offset for the value of the {\bf ra} column that
  makes it easy to build multiple components of objects that all reference a
  common {\bf ra}.}

\item{ {\bf $\Delta \delta$:}  The value of the declination offset in radians.  This
  can be used either for weak lensing or objects that moved from another
  exposure if you do not want to change the source position in the first two columns.
  $\Delta \delta$ is a small offset for the value of the {\bf dec} column that
  makes it easy to build multiple components of objects that all reference a
  common {\bf dec}.}

\item{{\bf spatial\_modelname:}   The name of the spatial model to be used, which
  are coded in the Monte Carlo.    We have currently implemented the following spatial
  models:  

  \begin{itemize}
  \item{{\bf point (no parameters):}  This is a model primarily used for stars, but
  also unresolved objects.}

  \item{{\bf gaussian (sigma in arcseconds):}  This is a model for a
  gaussian-shaped object.}

  \item{{\bf movingpoint:} (the derivative of the velocity arcseconds per
  second along the ra direction, the derivative of the velocity in arcseconds
  per second along the dec direction)} 

  \item{{\bf sersic2D:} (size of axis 1 in arcseconds, size of axis 2
  in arcseconds, position angle in radians, sersic index)}

  \item{{\bf sersic:} (size of axis 1 in arcseconds, size of axis 2
  in arcseconds, size of axis 3 in arcseconds, sersic index, polar angle in
  radians, position angle in radians)}

  \item{{\bf image models:}  If the
  spatial model name is a string which contains ``fits'' or ``fit'' the model
  is assumed to be a spatial image.  Then there are two parameters for this
  model:  pixel size (in arcseconds) and rotation angle (in degrees).}
   \end{itemize}  

}

\item{{\bf spatial\_parameters:} The associated parameters for each spatial model.
There could be none or many.  While the parser is reading the model it looks
for more parameters based on the name of the model.}

\item{{\bf Rest\_Frame\_Dust\_modelname:}  This is either the ccm for the CCM model,
or calzetti for the calzetti model.  If no dust model is desired, then put
``none'' for this field.}

\item{{\bf dust\_parameters:}   The parameters for both the calzetti and CCM are the
$A_v$ followed by the $R_v$ value.  If no dust model is used, do not use parameters}

\item{{\bf Lab\_Frame\_Dust\_modelname:}  This is either the ccm for the CCM model,
or calzetti for the calzetti model.  If no dust model is desired, then put
``none'' for this field.}

\item{{\bf dust\_parameters:}   The parameters for both the calzetti and CCM are the
$A_v$ followed by the $R_v$ value.  If no dust model is used, do not use parameters}

\end{itemize}


\vspace{0.5cm}
\noindent
{\bf Non-standard ways of using this format:}  We have also previously considered the concept of a data
cube consisting of a series of images in small wavelength bands (several times
smaller than a filter width).  This approach is not optimal for a photon
Monte Carlo of many sources, since this quickly grows too large because areas where there are
no sources have many data points.  However, this approach is useful for certain special applications.
Technically, this format is supported in two different ways.  First,
one can use a series of images for a single source and have each one have a
piece of the SED.  Second, one can represent an extended source with a complex spatially
dependent spectrum by a series of closely spaced point sources each with their own SED.  

It is also possible to use a single truth image and a single SED for the entire
field, and therefore just have a single entry of the catalog.  This does not
take full advantage of the wavelength-dependence capabilities in the Monte Carlo.
In practice for a full LSST field of view a mosaic of images each modest in size would be practical
as compared to s single extremely large image.

\subsection{SED files}

  The SED files are gzipped ASCII
files containing two columns: wavelength in nm and flux in units of
$ergs/cm^2/s/nm$.  A variable non-uniform wavelength spacing is allowed
with arbitary start and end wavelengths.
The flux is renormalized by the information in the instance catalog
as the file is read, therefore the normalization the SED file is arbitrary.
The SED is linearly interpolated within a wavelength bin.
The gridding is arbitrary to accomodate any required resolution for the
wavelength dependent structure.

\section{Output}

There are two forms of output for the simulator:  1) images and 2) additional
diagnostic data.

\subsection{Images}

The most important output of the simulator is very straight-forward:   a set of gzipped FITS
files containing the ADU counts in each pixel for each amplifier.
Calibration images (darks, flats, and biases) are also produced when the simulator is run in certain configurations.

\subsection{Diagnostic Data}

In the photon simulator, we also have the luxury of producing intermediate
diagnostic data that would be impossible for a real telescope could produce.
These have proven useful in validation, and also in DM algorithm development
This includes:  1) centroid files (mean positions and number of photons for each
astronomical object separately), 2) throughput files (counts of photons at every
optical surface), 3) event files (locations of all the photon ray hits in 3-D),
and 4) eimages (electron images before the background and detector defects
and electronic effects are applied).
We can easily accomodate new formats of diagnostic data in the future.  All of the
validation plots in this document use either the raw images or various kinds of
diagnostic data.

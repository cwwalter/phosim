\chapter{Atmosphere}

The atmosphere model consists of a continuous model for the distribution of atmospheric molecules
and a series of discrete layers of thin 2D screens that approximate the integrated effect of the
nearbuy 3D layer of the air.
Some screens have a fixed pattern atmospheric turbulence
and other have a cloud pattern.  The raytrace follows the photon from the top to the bottom of the
atmosphere.  We discuss first how the screens are constructed and positioned,
then discuss how the photons interact with the screens, and describe
the physics of propagation in the volume between the screens.

\section{Screen \& Bulk Geometry}

\subsection{Ray Intercept Calculation \& Layer Geometry}

The geometric model of the atmosphere consists of a series of screens and the
molecular air model located between the screens.  The screens are arranged
perfectly parallel to the ground, making the calculation of the ray
intercepts a simple analytic calculation.
The screens represent both the turbulence and the clouds that comprise
each 3D thin layer of the atmosphere.


\subsection{Turbulence Screens}

The screens of atmospheric turbulence follow the well-studied frozen-screen
approximation, meaning they do not change their character as they drift over
different regions of the atmosphere.  Different photons arrive at different
times, so even if they have the exact same path they may hit different parts
of the screens. The approach is a known accurate way to simulate the time
dependence of the physics.

\subsubsection{Number of Layers and Spacing}

\noindent
{\it Reference: \cite{young}}
\vspace{0.5\baselineskip}

We use 20 layers of atmospheric turbulence arranged logarithmically in height
between 20 m and 20 km.  20 km is a reasonable upper limit where the
atmosphere is likely to have a negligible impact on the observed seeing.  20
layers was determined to most accurately represent the degree of decorrelation
of typical atmosphere using wind data (see M. Young document).  We assume the
screens are independent,so having more than 20 screens would falsely represent
independent layers, and less than 20 layers would cause incomplete averaging of the photon refraction.

\subsubsection{Pixel Scale}

The screens are required to cover a horizontal region of about 1 km, in order
to cover all paths that a photon could intersect the screens during a typical
LSST exposure.  On the small scale, the screen should be able to represent the
turbulence on the 1 cm scale.  In order to have meet reasonable memory and
file size requirements, we found that we needed to construct the screen out of
linear superpositions of three different pixel sizes: 1 cm, 10 cm, and 1 m.
The screens all are 1024 by 1024 pixels, and the screens are toroidally wrapped when a
pixel is accessed off the edge of a screen.  The pixel sizes are constructed
so the largest pixel screen (1.024 km across)
will not wrap during two 15s exposures.


\subsubsection{Kolmogorov Spectrum}

\noindent
{\it Reference:  \cite{kolmogorov1941}}
\vspace{0.5\baselineskip}

The screens are generated to model a density perturbation in every pixel.
A Kolmorogov spectrum is used where the power as a
function of inverse spatial wavelength decreases to power of $-\frac{5}{3}$ (3D index).  Thus,
the largest power is on the largest spatial scales.  We flatten the power at the
outer scale which matches the expected form of the spectrum for shear driven
turbulence in air.
We set the power to zero at the outer-outer scale to sensibly limit the
physical form of the power on extremely large scales.
The inner scale would normally be set to the viscous scale.
However since the Kolmogorov spectrum rapidly falls at small scales we
cutoff the power at 1 cm without effecting the path of any photons.
We compute the gradient of the scalor density screen which yields a
2D vector screen of kicks that model the refractive effects of the variations
in the density that are enduced by the turbulent flow.
We also must filter the spectrum of the density and subsequent vector gradient
with a Bessel function which depends on the Fried parameters r0 (implies the value
of seeing). This extra filter step approximates the wavelength dependent effects that
forms the expected speckle pattern. 

\subsubsection{Outer Scale}

\noindent
{\it Reference: \cite{bocass2004}; \cite{beland1988}; \cite{coulman1988}
   \cite{abahamid2004}; \cite{young}}
\vspace{0.5\baselineskip}

We use the outer scale model which predicts an outer scale a function
of height, and match the mean parameters to the LSST site data of Bocass (2004)
where a median outer scale of 26.7 m and mean outer scale of 35.6 m. 
The mean and median can be used to construct a log-normal
distribution.  To add altitude-dependence, we scale the mean as a function
of height by scalings found in Abadhamid et al. (2004) and references therein
where the turbulence weighted median values are 1.27.


$$ L_0 = \left\{ \begin{array}{ll}
 3.21 h^{-0.11} & h < 2 km \\
\frac{4.0}{1.0+\left(\frac{h-8500}{2500} \right)^2} &  2 km < h < 17 km \\ 
 0.307-0.0324 \left( \frac{h}{1000}-17\right) & \\
  +0.00167\left(
\frac{h}{1000}-17 \right)^2+0.000476 \left(\frac{h}{1000}-17 \right)^3 &  h> 17 km \\
\end{array}  \right.
$$

Note, however, that estimates for the outer scale and its distribution vary
considerably in the literature.

\subsubsection{Outer-Outer Scale}

We set the power of the density fluctuations to zero at 500 m for a screen size of 1 km.
This parameter (newly invented here) is needed to set the outer most physical scale so the the
overall model will pass a convergence test. The flat power spectrum that drives the Kolmogorov
spectrum cannot sensibly extend to arbitary large scales. Since the aperture of LSST is much
smaller than 500m the simulated paths of photons are not too sensitive to this unmeasured parameter.

\subsubsection{Relative Turbulence Intensity}

\noindent
{\it Reference:  \cite{vernin2000}; \cite{young}}
\vspace{0.5\baselineskip}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{turbulence_height.ps}
\end{center}
\caption{The mean turbulence intensity vs. height.  The 4 points at each layer
are the seasonal variation.}
\end{figure}

We use a Weibull distribution for the relative turbulence in each layer.  The
refractive index structure constant sigma values ($m^{-\frac{2}{3}}$) are
given below.  We have a seasonal model, so that the turbulence follows a
realistic distribution based on LSST site data.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|r|r|}
\hline
meters & January &April	 &  July &  October \\ \hline
23000 & $0.00 \times 10^{+00}$ & $0.00 \times 10^{+00}$ & $0.00 \times 10^{+00}$ & $0.00 \times 10^{+00}$ \\ \hline
22000 & $0.00 \times 10^{+00}$ & $0.00 \times 10^{+00}$ & $0.00 \times 10^{+00}$ & $0.00 \times 10^{+00}$ \\ \hline
21000 & $1.20 \times 10^{-18}$ & $8.78 \times 10^{-19}$ & $0.00 \times 10^{+00}$ & $8.78 \times 10^{-19}$ \\ \hline
20000 & $4.79 \times 10^{-18}$ & $1.76 \times 10^{-18}$ & $9.57 \times 10^{-19}$ & $1.60 \times 10^{-18}$ \\ \hline
19000 & $7.18 \times 10^{-18}$ & $3.19 \times 10^{-18}$ & $2.39 \times 10^{-18}$ & $2.31 \times 10^{-18}$ \\ \hline
18000 & $6.38 \times 10^{-18}$ & $4.79 \times 10^{-18}$ & $3.99 \times 10^{-17}$ & $2.79 \times 10^{-18}$ \\ \hline
17000 & $3.99 \times 10^{-18}$ & $2.71 \times 10^{-18}$ & $1.20 \times 10^{-17}$ & $3.99 \times 10^{-18}$ \\ \hline
16000 & $2.39 \times 10^{-18}$ & $3.03 \times 10^{-18}$ & $1.60 \times 10^{-17}$ & $4.39 \times 10^{-18}$ \\ \hline
15000 & $2.39 \times 10^{-18}$ & $3.99 \times 10^{-18}$ & $1.36 \times 10^{-17}$ & $3.99 \times 10^{-18}$ \\ \hline
14000 & $2.39 \times 10^{-18}$ & $3.99 \times 10^{-18}$ & $6.38 \times 10^{-18}$ & $2.39 \times 10^{-18}$ \\ \hline
13000 & $1.76 \times 10^{-18}$ & $4.79 \times 10^{-18}$ & $4.79 \times 10^{-18}$ & $1.76 \times 10^{-18}$ \\ \hline
12000 & $1.44 \times 10^{-18}$ & $4.31 \times 10^{-18}$ & $3.27 \times 10^{-18}$ & $2.39 \times 10^{-18}$ \\ \hline
11000 & $3.27 \times 10^{-18}$ & $7.58 \times 10^{-18}$ & $4.79 \times 10^{-18}$ & $3.99 \times 10^{-18}$ \\ \hline
10000 & $3.19 \times 10^{-18}$ & $6.38 \times 10^{-18}$ & $5.66 \times 10^{-18}$ & $6.62 \times 10^{-18}$ \\ \hline
9000 & $5.19 \times 10^{-18}$ & $1.20 \times 10^{-17}$ & $1.04 \times 10^{-17}$ & $4.79 \times 10^{-18}$ \\ \hline
8000 & $7.18 \times 10^{-18}$ & $8.78 \times 10^{-18}$ & $1.04 \times 10^{-17}$ & $4.87 \times 10^{-18}$ \\ \hline
7000 & $2.55 \times 10^{-17}$ & $7.98 \times 10^{-18}$ & $1.44 \times 10^{-17}$ & $8.78 \times 10^{-18}$ \\ \hline
6000 & $3.19 \times 10^{-17}$ & $1.36 \times 10^{-17}$ & $3.03 \times 10^{-17}$ & $2.07 \times 10^{-17}$ \\ \hline
5000 & $2.15 \times 10^{-17}$ & $1.60 \times 10^{-17}$ & $1.76 \times 10^{-16}$ & $2.71 \times 10^{-17}$ \\ \hline
4000 & $4.39 \times 10^{-17}$ & $3.59 \times 10^{-17}$ & $1.44 \times 10^{-16}$ & $5.98 \times 10^{-17}$ \\ \hline
3000 & $7.18 \times 10^{-17}$ & $1.52 \times 10^{-16}$ & $1.44 \times 10^{-16}$ & $5.59 \times 10^{-17}$ \\ \hline
2000 & $9.57 \times 10^{-17}$ & $6.38 \times 10^{-17}$ & $1.68 \times 10^{-16}$ & $1.12 \times 10^{-16}$ \\ \hline
\end{tabular}
\end{center}
}
\caption{The average turbulence intensity for various altitude as a function
  of season for the LSST site.}
\end{table}

\subsubsection{Absolute Turbulence Intensity}

The turbulence values listed above predict an absolute level of seeing.
However, we iteratively choose values for the structure function until we
reproduce the seeing value input from the operation simulation.  The value of
turbulence intensities are then renormalized to produce the exactly predicted values.
This assures us that we have the same seeing distribution as in the operations simulator.
Alternately we can use the seasonal model of the structure function of the
model to randomly select a structure profile independent of {\it OpSim}.
Data challenges use the {\it OpSim} provided values for total seeing.
Special studies of stacks of LSST images can use the internal structure
funnction model and the associated integrated seeing values.

\subsubsection{Density Screen vs. Screen Derivatives}

The largest scale density screen is kept for the opacity calculation, and the
derivative of density of all three screens is used for the refraction
calculation.  The derivative is calculated using a simple 2-point derivative.
In the figure below, we show the density of the coarse screen, and the
derivative of the screen on three different scales.


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=2.75in]{density.ps}
\includegraphics[width=2.75in]{coarse.ps}
\includegraphics[width=2.75in]{medium.ps}
\includegraphics[width=2.75in]{fine.ps}
\end{center}
\caption{Example of Kolmorogov turbulence screens.  The top left shows the
  density on a coarse scale, the top right shows the derivative, the bottom
  left shows the derivative on a medium scale, and the bottom right shows the
  screen on the smallest scales.}
\end{figure}

\subsection{Cloud Screens}


\noindent
{\it Reference: \cite{ivezic2007}}
\vspace{0.5\baselineskip}

The cloud model consists of an absorptive probability for every pixel, and the
photon has some probability of being absorbed by a photon as it propagates
through the atmosphere.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=2.75in]{cloud_top.ps}
\includegraphics[width=2.75in]{cloud_bottom.ps}
\end{center}
\caption{Examples of the cloud screens showing the variation of opacity on the
  top layer (left) and bottom layer (right).}
\end{figure}

\subsubsection{Number of Layers}

We use two clouds layers:  one placed at the highest altitude of the
turbulence screens and one place at the mid point of the turbulence screens.  

\subsubsection{Correlation Length}

\noindent
{\it Reference:  \cite{ivezic2007}}
\vspace{0.5\baselineskip}

The cloud screens have an exponential structure function with a angular coherence scale of 2
degrees to be consistent with the measurements of Ivezic based on SDSS wide field
images. This fixed angular scale is the reason that the images of the two
cloud layers show a fine structure for the bottom layer and a coarse structure
for the higher layer. Clouds of a fixed angular size will have a spatial size proportional
to the height of the cloud layer. This is not a high fidelity model of actual
clouds layers. We have not implemented a model that tracks the variations in cloud
structure as function of height. The data is not readily available. Also the structure
function estimated by Ivezic is assumed to be isotropic. Clouds clearly have non-isotropic
structure that could be related to the wind direction. We decided on two layers of
clouds each with a different wind velocity to create a partly realistic complexity that
would at least mimic the difficulties for photometric calibration.  Additional
work is needed (see part IV).

\subsubsection{Pixel Scale}

The pixel scale of the cloud screen is 1 m (the same size as the coarse
turbulence screen).


\subsection{Wind Direction \& Speed}

\noindent
{\it Reference:  \cite{noaa}; \cite{young}}
\vspace{0.5\baselineskip}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{wind_height.ps}
\end{center}
\caption{The sigma for the wind speed Weibull distribution parameter vs. height.  The 12 points at each layer
are the seasonal variation.}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{wind_dir_height.ps}
\end{center}
\caption{The average wind direction (degrees from east) vs. height.  The 12
  points at each layer are the seasonal variation.  The lines show the
  variation in the wind directions.  The wind direction is fairly anisotropic.}
\end{figure}

In order to determine where a photon hit a screen at a given layer, we first
calculate the x and y position.  We then calculate the pixel in the
appropriate screen given two components of the wind vector for each screen.
The arrival time of the photon then dictates exactly which pixel is used.  For
the wind model, we have a wind distribution that varies as a function of
altitude.  Both the wind direction and magnitude have a seasonal distribution
at the LSST site.  Using the NOAA NCEP/NCAR Reanalysis Monthly Database at the
longitude and latitude of LSST, we fit the historical data to a Weibull
distribution (k=2) for the wind velocity.  The wind magnitude sigma
values (m/s) are given below and shown in the figure.  The wind measurements are a function of the
pressure in the atmosphere which is converted to corresponding height.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|r|r|r|r|r|r|r|r|r|r|}
\hline
 &  Jan & Feb & Mar & Apr & May & June & July & Aug & Sep & Oct & Nov & Dec \\ \hline
 10mb & 18.72 & 18.52 & 9.29 & 5.12 & 16.11 & 22.79 & 20.25 & 14.81 & 11.7 & 6.32 & 5.39 & 11.22 \\ \hline
20mb & 8.45 & 9.62 & 6.7 & 2.35 & 8.64 & 12.85 & 11.22 & 8.47 & 6.87 & 3.78 & 3.24 & 5.32 \\ \hline
30mb & 8.7 & 9.51 & 5.71 & 1.99 & 6.3 & 9.3 & 8.31 & 6.96 & 5.68 & 3.13 & 2.78 & 4.49 \\ \hline
50mb & 2.83 & 3.64 & 1.78 & 2.83 & 6.88 & 9.98 & 9.26 & 8.22 & 6.47 & 3.76 & 2.23 & 1.68 \\ \hline
70mb & 2.94 & 2.62 & 3.69 & 6.95 & 10.1 & 12.25 & 11.68 & 11.24 & 9.81 & 7.82 & 6.02 & 4.69 \\ \hline
100mb & 10.32 & 9.45 & 11.15 & 14.42 & 16.96 & 18.42 & 17.63 & 17.45 & 16.59 & 15.89 & 14.5 & 12.87 \\ \hline
150mb & 17.99 & 17.21 & 18.57 & 20.76 & 23.51 & 25.76 & 25.18 & 25.22 & 25.21 & 25.35 & 22.78 & 20.46 \\ \hline
200mb & 18.81 & 18.34 & 19.3 & 20.83 & 23.65 & 26.35 & 25.99 & 26.55 & 26.79 & 27.04 & 23.54 & 20.7 \\ \hline
250mb & 16.9 & 16.63 & 17.37 & 18.56 & 21.32 & 24.06 & 23.71 & 24.51 & 24.66 & 24.84 & 21.16 & 18.21 \\ \hline
300mb & 14.53 & 14.3 & 15.04 & 16.07 & 18.66 & 21.17 & 20.84 & 21.63 & 21.64 & 21.8 & 18.38 & 15.59 \\ \hline
400mb & 9.89 & 9.7 & 10.61 & 11.76 & 13.96 & 15.99 & 15.78 & 16.26 & 16.19 & 16.1 & 13.4 & 11.03 \\ \hline
500mb & 6.49 & 6.31 & 7.29 & 8.48 & 10.31 & 11.97 & 11.85 & 12.06 & 11.94 & 11.59 & 9.48 & 7.59 \\ \hline
600mb & 4.12 & 4.04 & 4.99 & 6.06 & 7.49 & 8.89 & 8.84 & 8.8 & 8.53 & 7.96 & 6.29 & 4.99 \\ \hline
700mb & 2.33 & 2.34 & 3 & 3.88 & 5.14 & 6.26 & 6.23 & 5.88 & 5.41 & 4.75 & 3.57 & 2.86 \\ \hline
850mb & 1.06 & 1.21 & 1.37 & 1.7 & 2.16 & 2.77 & 2.77 & 2.43 & 2.1 & 1.69 & 1.25 & 1.2 \\ \hline
925mb & 1.25 & 1.05 & 0.92 & 1.26 & 1.82 & 2.45 & 2.46 & 2.17 & 1.86 & 1.51 & 1.33 & 1.31 \\ \hline
1000mb & 1.27 & 1.09 & 0.92 & 1.19 & 1.67 & 2.19 & 2.18 & 1.94 & 1.67 & 1.48 & 1.36 & 1.34 \\ \hline
\end{tabular}
\end{center}
}
\caption{The magnitude of the wind vector as a function of pressure (and
  therefore height) and month of the year for the LSST site.}
\end{table}

\noindent
We use a gaussian distribution for the wind direction, which is very
anisotropic.  The wind direction means (degrees from East) are given below.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|r|r|r|r|r|r|r|r|r|r|}
\hline
 &  Jan & Feb & Mar & Apr & May & June & July & Aug & Sep & Oct & Nov & Dec \\ \hline
10mb & 186.2 & 185.7 & 184.5 & 22.6 & 359.9 & 358.6 & 358.8 & 358.2 & 353.6 & 331.3 & 206.8 & 187.2 \\ \hline
20mb & 162.5 & 167.2 & 172.6 & 21.3 & 0.5 & 354.9 & 356.6 & 353.8 & 351 & 352.1 & 134.1 & 157.7 \\ \hline
30mb & 187.1 & 184.9 & 189.2 & 18.1 & 359.9 & 357.4 & 359.3 & 358.9 & 355 & 346.1 & 214.7 & 199.7 \\ \hline
50mb & 165.8 & 175.9 & 176.2 & 6.5 & 3.7 & 1 & 3.4 & 2.9 & 1.2 & 6.1 & 19.7 & 114.6 \\ \hline
70mb & 331.6 & 316.9 & 335.4 & 350.2 & 353.7 & 353.9 & 356 & 356.2 & 353.9 & 352.7 & 350.1 & 340.4 \\ \hline
100mb & 347.5 & 345.3 & 347.6 & 351.6 & 354.5 & 354.2 & 354.6 & 354.5 & 352.1 & 351.2 & 350.9 & 348.2 \\ \hline
150mb & 348.4 & 349.1 & 349.1 & 351.7 & 355.6 & 356.4 & 357.1 & 357 & 354.7 & 352.3 & 350.4 & 346.4 \\ \hline
200mb & 349.8 & 350.4 & 349.9 & 352 & 355.3 & 356 & 356.4 & 356.5 & 353.8 & 351.6 & 350.9 & 347.5 \\ \hline
250mb & 350.9 & 351.4 & 350 & 352.3 & 355.4 & 355.9 & 355.8 & 355.6 & 352.7 & 350.9 & 351.1 & 348.4 \\ \hline
300mb & 353 & 353.4 & 351 & 352.9 & 355.6 & 356 & 355.6 & 355.1 & 352 & 350.5 & 351.6 & 350.4 \\ \hline
400mb & 354.3 & 354.5 & 350.4 & 351.8 & 353.8 & 354.6 & 353.8 & 352.7 & 349.2 & 347.6 & 349.5 & 350.7 \\ \hline
500mb & 350.2 & 350.5 & 346.9 & 348.7 & 351.1 & 352.5 & 351.7 & 350.2 & 346 & 343.4 & 344.7 & 346.5 \\ \hline
600mb & 336.7 & 334.7 & 335.3 & 340.9 & 346 & 348.3 & 347.4 & 345.7 & 340.5 & 336.5 & 335.8 & 335.3 \\ \hline
700mb & 328.9 & 319.4 & 320.3 & 327.9 & 334.6 & 338.2 & 336.9 & 335.8 & 330.7 & 327.6 & 328.4 & 328.4 \\ \hline
850mb & 299.9 & 273.6 & 274.4 & 282.6 & 295.8 & 305.4 & 305.3 & 305.6 & 304.9 & 304.4 & 303.1 & 304.5 \\ \hline
925mb & 46.5 & 37.3 & 348.1 & 305.7 & 301.7 & 305 & 305.5 & 310.7 & 318.7 & 338.7 & 21.1 & 34.5 \\ \hline
1000mb & 43.7 & 40 & 4.5 & 302.8 & 300.2 & 303.3 & 304.6 & 312.8 & 323.4 & 347.1 & 25.4 & 34 \\ \hline
\end{tabular}
\end{center}
}
\caption{The average direction of the wind vector as a function of pressure (and
  therefore height) and month of the year for the LSST site.}
\end{table}

The wind direction gaussian standard deviations (degrees) are given below.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|r|r|r|r|r|r|r|r|r|r|r|}
\hline
 &  Jan & Feb & Mar & Apr & May & June & July & Aug & Sep & Oct & Nov & Dec \\ \hline
10mb & 1.03 & 1.15 & 5.24 & 33.14 & 12.06 & 3.78 & 4.03 & 9.03 & 11.88 & 63.18 & 36.16 & 2.47 \\ \hline
20mb & 2.63 & 2.48 & 5.58 & 86.02 & 10.06 & 5.3 & 5.77 & 18.54 & 36.32 & 89.97 & 81.64 & 9.14 \\ \hline
30mb & 6.44 & 6.04 & 7.96 & 71.49 & 10.85 & 6.62 & 6.07 & 16.19 & 38.27 & 92.19 & 77.01 & 16.49 \\ \hline
50mb & 37.12 & 16.38 & 51.85 & 27.44 & 9.56 & 5.91 & 6.38 & 7.38 & 12.82 & 40.96 & 69.57 & 69.05 \\ \hline
70mb & 38.52 & 50.44 & 17.71 & 8.69 & 9.06 & 6.87 & 6.02 & 5.38 & 6.23 & 7.25 & 11.18 & 19.9 \\ \hline
100mb & 9.33 & 13.43 & 9.13 & 7.71 & 8.22 & 6.95 & 5.65 & 5.44 & 5.94 & 6.26 & 6.15 & 8.06 \\ \hline
150mb & 9.17 & 12.17 & 8.6 & 7.76 & 8.19 & 6.54 & 4.93 & 5.87 & 5.72 & 6.05 & 6.44 & 8.21 \\ \hline
200mb & 8.9 & 11.89 & 9.04 & 8.34 & 8.81 & 6.75 & 5.27 & 6.56 & 5.91 & 6.31 & 6.88 & 7.96 \\ \hline
250mb & 8.48 & 11.46 & 9.18 & 8.91 & 9.18 & 6.96 & 5.97 & 7.39 & 6.18 & 6.48 & 7.22 & 7.77 \\ \hline
300mb & 7.91 & 11.43 & 9.33 & 9.53 & 9.42 & 7.17 & 6.65 & 7.89 & 6.44 & 6.63 & 7.41 & 7.58 \\ \hline
400mb & 7.65 & 12.09 & 9.69 & 10.3 & 9.98 & 7.86 & 7.93 & 8.74 & 7.14 & 6.96 & 8.16 & 8.33 \\ \hline
500mb & 8.33 & 14.27 & 10.54 & 10.25 & 10.74 & 8.81 & 9.15 & 9.51 & 7.91 & 7.18 & 9.27 & 9.56 \\ \hline
600mb & 13.05 & 18.74 & 12.03 & 10.88 & 11.66 & 10.28 & 10.62 & 9.92 & 8.72 & 8.34 & 10.25 & 13.26 \\ \hline
700mb & 25.88 & 30.15 & 17.85 & 13.15 & 11.21 & 12.02 & 10.97 & 9.58 & 9.97 & 11.58 & 14.8 & 20.26 \\ \hline
850mb & 67.73 & 68.39 & 32.42 & 25.12 & 18.96 & 15.65 & 12 & 12.5 & 14.46 & 29.59 & 63.9 & 70.25 \\ \hline
925mb & 47.24 & 68.6 & 70.99 & 65.41 & 32.57 & 17.25 & 12.42 & 24.66 & 30.25 & 48.54 & 58 & 58.02 \\ \hline
1000mb & 44.91 & 63.1 & 68.12 & 74.35 & 31.36 & 19.87 & 14.63 & 29.28 & 34.2 & 54.47 & 56.67 & 58.62 \\ \hline
\end{tabular}
\end{center}
}
\caption{The variation of the direction of the wind vector as a function of pressure (and
  therefore height) and month of the year for the LSST site.}
\end{table}


\subsection{Bulk Atmosphere Model}

\noindent
{\it Reference: \cite{bodhane1999}; \cite{green1988}; \cite{liou2002}; \cite{grace}}
\vspace{0.5\baselineskip}

We track four components represent the bulk air located between the screens:
the density of the atmosphere vs. height, the density of water vapor
vs. height, the density of molecular oxygen vs. height, and the density of
ozone vs. height.  From the literature we constructed the density distribution
in the plot below.  We use the altitude of 2660 m for the location of LSST.
These density distributions are used to determine the opacity as we describe
in the opacity section.  For the airmass dependence factor we use $
\frac{1}{\cos{z} + 0.025 e^{-11 \cos{z}}}$.  This factor then scales the
density distributions below.

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{atm_density.ps}
\end{center}
\caption{The atmospheric density distributions used in the opacity
  calculation.  The black shows all molecular components, the
  green shows the ozone, the red shows the molecular oxygen, and the blue shows the water vapor.}
\end{figure}

\section{Interactions with the Screens and Bulk Atmosphere}

\subsection{Refraction}

Once the screen pixel location is determined for a given photon, we use the
derivative of the turbulence to perturb the direction of a photon.  This is
the simple major interaction that will determine the seeing and contribute to
the atmospheric PSF.  The derivative is first normalized to the seeing at
zenith at 500 nm, which is then input from the operational parameters.  It is then scaled by two factors:  the
zenith dependence of the seeing ($\cos{\mbox{zenith}}^{\frac{-3}{5}}$) and the
wavelength dependence of the refraction
($64.328+\frac{29498.1}{146-\lambda_{\mu m}^{-2}}+\frac{255.4}{41-\lambda_{\mu m}^{-2}}$
divided by the same factor evaluated at 0.5 $\mu m$.  


\subsection{Cloud Grey Opacity}

\noindent
{\it Reference:  \cite{ivezic2007}}
\vspace{0.5\baselineskip}

When a photon hits a cloud screen pixel it has some probability of being
absorbed (destroyed or lost by scattering outside the aperture).
We use an average opacity of 0.1 magnitudes for each of
two clouds screens, and a variation of 0.035 magnitudes to set the amplitude of
variation from the screens.  We vary each of these numbers by 1\% from one
atmosphere to the next. These values are typical of good conditions but are
not really based on any database of measurements similar to what we use
for wind data. Just as the spatial structure of the clouds needs
more work so does the opacity model. For many purposes data can be used for
science grade work with opacities up to $\sim$1 magnitude of lost signal.
Our current cloud model has an average total opacity of $\sim$0.2 magnitudes of lost signal
which is independent of wavelength (grey response).
The cloud pattern moves with the same wind velocity as the turbulent screen at the
same height. The cloud spatial structure has a resolution of 1m, which is the same as the
coarse turbulence screen. 

\subsection{Opacity of the Atmosphere}

\noindent
{\it Reference: \cite{rothman2009}; \cite{sander2006}; \cite{thomas1999};
  \cite{hitran}; \cite{grace}}
\vspace{0.5\baselineskip}

The opacity of the atmosphere in the photon raytrace is determined by calculating the local
optical depth for each segment of the photons path.  Between each atmospheric
screen the column density of each molecular species of the atmosphere is
calculated by integrating the density profiles in our bulk atmosphere model and the
path length the photon.  This column density is then modulated by two
factors.  First, the overall column density is multiplied by a random gaussian
(or log normal) with mean of 1 and $\sigma$ given in the Table below for each
component.  Second, the local column density is also perturbed by a factor of
1\% $\times \sqrt{N_{\mbox layers}}$ of the relative density of the height at
each screen.  In this way, the opacity will vary slightly from exposure
to the next, and it will vary across the field in a complex way.

After the local column density is determined the optical depth is calculated
by multiplying by the cross-sections as a function of wavelength from the plot
below.  The cross-sections are taken from the HITRAN atomic database as well
as the Rayleigh scattering cross-section.  The cross-sections are convolved
with appropriate thermal and natural line width for a temperature model of the
atmosphere that varies in height (see E. Grace technical note).  Thus, the
effective cross-section has a altitude dependence.  

The probability the photon is destroyed along its particular path segment is
then equal to $e^{-\tau(\lambda)}$ where $\tau = \sum_i \tau_i(\lambda) =
\sum_i \int \sigma_i (\lambda, h)  n_i(h) dh$.

\begin{table}[ht!]
{\tiny
\begin{center}
\begin{tabular}{|l|r|l|}
\hline
Type &  Variation & Distribution \\ \hline \hline
Total Density & 0.01 & gaussian \\ \hline
Molecular Oxygen & 0.01 & gaussian\\ \hline
Ozone & 0.01 & gaussian \\ \hline
Water vapor & 0.18 & log-normal \\ \hline 
\end{tabular}
\end{center}
}
\caption{Variation of components of the atmosphere.}
\end{table}


\begin{figure}[ht!]
\begin{center}
\includegraphics[width=5.5in]{atm_cs.ps}
\end{center}
\caption{The atmospheric opacity cross-sections used in the opacity
  calculation.  The black shows the rayleigh scattering cross-section, the
  green shows the ozone cross-section, the red shows the molecular oxygen
  cross-section, and the blue shows the water vapor cross-sections.}
\end{figure}



\subsection{Atmospheric Dispersion}

\noindent
{\it Reference: \cite{filippenko1982}}
\vspace{0.5\baselineskip}

We use a formula below from the literature for atmospheric dispersion, which
depends on air pressure (P, set to 520 mmHg), water vapor pressure (W, 8 mmHg),and
ground temperature (T, 5 degrees C).  This shifts the position of the photon
depending on wavelength by a small angle.  We subtract the mean positional
difference of the entire field, so a photon in the center of the field with
wavelength of 500 nm would receive no net dispersion.  This is equivalent to
the telescope operator knowing about the effect of atmospheric dispersion and
then setting the pointing accordingly.  However, photons of
different wavelength and field positions would receive net displacement
proportional to their distance from the zenith and in the direction to the
zenith.  The angular offset is given by the equation below,

 
$$ 
\left( 64.328+\frac{29498.1}{146-\frac{1}{\lambda^2}}+\frac{255.4}{41-\frac{1}{\lambda^2}} \right)
     P \frac{1+\left(1.049-0.0157 T\right) 10^{-6} P }{720.883
       \left(1+0.003661 T
       \right)}-\frac{0.0624-\frac{0.000680}{\lambda^2}}{(1+0.003661 T)W} $$



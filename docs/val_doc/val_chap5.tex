\chapter{Physics Model Improvements}

Below is a list of planned future updates to the photon
simulator.  In some cases, extensive research has already been done, and in
other cases we have done limited preliminary work.  We roughly list them in
the same order as the physics description document. 


\section{Astrophysical Model Improvements}

\begin{itemize}

  \item{{\bf Galaxy Morphology:}  The current galaxy model uses ten parameter
    to specify a separate ellipsoidal sersic distribution for both a galaxy's
    bulge and its disk.  The next step would be to add a slightly more complex
    morphology by representing spiral arms and non-ellipsoidal shapes.}

  \item{{\bf More flexible SED interface:}  To represent a greater diversity of
    objects and types of stars and galaxies, we probably need to move to a
    system where SEDs are represented by additive components in order to keep
    the total number of SEDs to a reasonable number (10,000).  This requires
    re-working the interface, and modifying the method of choosing wavelengths of photons.}

   \item{{\bf Other features:}  Some features such as the weak lensing shearing and
     simulating galaxies from truth images are tested in a limited sense, but
     have not been deployed on the full grid computing system.  We expect the implementation
     of these will be straight-forward, but could require some changes in the
     interfaces.}

     \item{{\bf Continued OpSim interface:}  We will continue to use the OpSim
       parameters as input along with the astrophysical parameters, and will
       continue to evolve the parameters so that interface is as self-consistent as possible.}


\end{itemize}

\section{Atmosphere Model Improvements}

\begin{figure}[ht!]
\label{IRcloud}
\begin{center}
\includegraphics[width=6.4in]{figure_IRcloud.ps}
\end{center}
\caption{Whole-sky cloud emissivity maps acquired and processed by the MAGNUM
  infrared cloud monitor under under various sky conditions: clear (top left),
  thin (top right), partially cloudly (bottom left), and entirely cloudy
  (bottom right).  Figure 9 is from Suganuma et al., Publications of the Astronomical Society of the Pacific, Vol. 119, No. 855, May 2007}
\end{figure}


\begin{itemize}


\item {{\bf Improved Turbulence Screen Model:}
The distributions for the outer scale (and also the
outer-outer scale) are very limited.  This model clearly has a significant
effect on the atmospheric PSF, so it is important to study not only the mean
values for these parameters, but their distributions as well.  The numbers of
layers has proved to be rather important in the distribution of PSF
properties, so it must be studied in detail about how many independent layers
(or a vertical correlation scale) should be used. With the
current model, we can show that the atmospheric PSF are reasonable but may not
be the ultimate model.  }


   \item{{\bf LSST Site parameter data:}  An extensive set of planned site
     measurements in the next few years can be incorporated into the
     simulator.  This includes study of the wind velocity vector vs. height,
     the outer sale vs. height, and the turbulence intensity vs height.  We
     have working reasonable models for all three, but it would be useful to
     study the correlations in time, the correlation scale height of each three, and the
     correlations of each distribution with each other.  These
     distributions affects the atmospheric PSF shape, size, correlation
     with angle, and correlation between one exposure and the next. }



   \item{{\bf Improved Cloud model:} The cloud model can be update based on
     LSST site data using IR images of actual clouds.  The current model used
     a simple structure function derived using SDSS data.  The number of effective layers, correlation
  scales, and variation from one exposure to the next can all be updated to
  produce a more realistic model. Also direct measurement of clouds over time
  will also provide information about the wind profile.  Figure \ref{IRcloud} shows an example of an whole sky IR image of clouds.}

\item{{\bf Improved Opacity model:}  The opacity model consist of atomic absorption of
  molecular oxygen, ozone, water vapor, as well as Rayleigh scattering.  The
  variation of some of these component is currently just a guess, and other trace
  components could be added, such as aerosols.  The correlations between their
  relative abundance and other atmospheric parameters is currently assumed
  independent, which may not be the case.    These could be updated with further study.}

\item {{\bf Improve Water Vapor Model:} Water vapor is included in the current model both in terms of emission in the sky background
and the absorption of light from celestial sources.  It is clearly the most
important component that has signficant variability (10s of percents).
Currently, we have a crude log normal variance of the water vapor valiability
based on measurement at various sites, and couple the emission in the
background to that variability. We really need better information about the
water vapor variability at the LSST site.  We also need to vary the emission
across the field in the background model.  This topic is particularly
important for the Z and Y band simulations.}


\end{itemize}

\section{Telescope/Camera Geometry \& Perturbation Improvements}

\begin{itemize}

\item{{\bf LSST geometric design updates}:  We will continue to track the basic
          design of LSST (optics design, detector layout, and coating
          specifications.  This will evolve in the project, and we will have
          to track these changes.}


\item{{\bf Dynamic camera geometry model to match dynamic telescope model:}  One
  change in choosing parameters for the optical perturbations is that some of
  the parameters describing the camera geometry are static (the same
  throughout the entire run) and most involving the optics are dynamic (change
every exposure).  We have to build the capability of modifying the parameters
like in the optics\_parameter code for the camera as well
without producing large data files.}

\item{{\bf Time-scales associated with each physics perturbation model:}  Once the
  two perturbation models are on the same level of fidelity, we then can set
  the perturbations to evolve on different time scales.  So two adjacent
  observations will have similar perturbations, but two observations separated
by a year will have a different realization entirely.  This is important,
because the mirror control system is expected to evolve on minute timescales.}

\item{{\bf Perturbation/Misalignment parameters predicted from physics models and
  build tolerances:}  The perturbations then can be set from models of physics
  (vibrational, thermal, gravity, pressure) with their own external parameters
  (e.g. the gravity vectors) or build tolerances (fabrication errors).  These
  models will be developed with engineers in the camera and telescopes teams.
  Therefore, we will end up replacing the single line gaussian random number
  called that sets the perturbations currently, with a several line code model
  that sets the parameters from a physics models with observing parameters.
  Note that many of the possible degrees of freedom are currently set to 0,
  which is only a crude approximation (i.e. the lens surfaces).}

\item{{\bf Perturbation/Misalignment parameters coupled through simulation of
  feedback loop:}  To truly address the correlations between the parameters,
  we have to simulate the feedback loop of generating wavefront images, and
  then simulating how  well the correction occurs through the active optics
  system.  The model would then use the detailed atmospheric conditions at the
  time of the observation.  It may be necessary to simulate in a crude sense
  the sequence of perturbations that may occur several minutes before an
  exposure to accurately predict what the perturbations will be during a
  particular exposure pair.  This sort of improvement will likely occur
  through study with the active optics group, and then we will be able to
  couple the correlated parameters   through some kind of simplified model.}


\item {{\bf New tracking model:}
The current tracking model has a white spectrum and is simulated independently
for each exposure pair.  We have sample power spectra for the
fine pointing system that is not white that could be implemented.  The
engineering tolerance of the tracking model needs further clarification from
the engineering team.}

\item {{\bf Filter changer:}
The cadence is effected by the filter changes. We may have repeatable slight errors
in filter locations after a filter change.  Its unclear how important this
effect is.}

\item{{\bf Large angle scattering physical model:}   Our current large angle
  scattering model is an empirical distribution in angle determined from
  Gemini data.  We could replace this empirical model by a real physical model
with a spectrum of perturbations and then predict the angular distribution
directly from scalar diffraction theory.}

\end{itemize}

\section{Telescope/Camera Interactions Improvements}

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.4in]{figure_sweeney.eps}
\end{center}
\caption{\label{surface} From: "Surface characterization of optics for EUV lithography", P. Gaines, D. W.  Sweeney, K. W. DeLong, S. P. Vernon,
S. L. Baker, D. A. Tichenor, and R.  Kestner, LLNL, 1996 }
\end{figure}

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.4in]{figure_new_filters.eps}
\end{center}
\caption{\label{filters} New Filter designs from Vendors}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\includegraphics[width=6.4in]{figure_ghost.eps}
\end{center}
\caption{\label{ghost}
Left panel is a calculation of the Ghost pattern of a bright star with the
photon simulator;
the right panel is the ghost pattern derived from an independent investigation
by the LSST engineering team.  These images are similar but with differences that require further evaluation.
 }
\end{figure}


\begin{itemize}



\item{{\bf Verification of multi-layer approximation:}  We use a multi-layer
  approximation to simulate the effect of the angle dependent transmission in
  the filter coatings, lens coating, and the detector coatings.  In this
  model, we have an approximate formula for the angular dependence as well as
  an effective index of refraction in the multi-layers.  Alternatively, the
  exact angle and wavelength transmission function can be calculated if the
  exact multi-layer prescription (composition and height pattern) is known, and
the reflective and transmission probabilities can be calculated directly.  We
can then either verify our approximations, or replace this with a more
detailed model.}

\item{{\bf Coating non-uniformity:}  The coatings currently have no positional
  dependence, but we could include such an effect where the coatings on the
  mirrors, filters, lenses, or detectors are not perfectly uniform.}

\item{{\bf Update to filter, lens, detector coating models:}  The filter, lens, and
  detector coating models need to be updated based on the actual design and
  not on requirements.  We
  have, for instance, no coating on the back of the filter, and the actual
  design may change.  This ultimately affects the accuracy of the simulation
  of photometry as well as the ghost patterns (see figure \ref{ghost}.  The
  back surface reflectivity currently has a simple 20\% wavelength-independent
  reflectivity that could be improved.}

\item{{\bf Glass non-uniformities:}  The glass has a single index of refraction in
  all of the filters and lenses.  We can vary this as a function of position
  according to engineering studies.  The pressure and temperature changes
  across L3, in particular, are predicted to have a noticeable change in the
  index of refraction throughout the L3 glass.  In addition, the could be
  surface pitting or macroscopic defects internal to the lenses, this could be
modeled by volume variation in the index of refraction.}

\item{{\bf Electron diffusion physics:}  The model of electron diffusion can be
  updated to include both the fact the the electron's velocity saturates in
  Silicon as well as the curved field lines exhibited in thick devices.  The
 tree ring effects identified by the engineering team are likely in the 3-D,
 which modifies the pixel boundaries.}


\item {{\bf Model for CCD traps}  This effect is thermally sensitive and is
  not currently part of the CCD model.  This has the potential to have a
  significant impact on PSF prediction and could imply a loss of some data for weak lensing.}

\item{{\bf More complex dome seeing models:}  Our dome seeing model is a simple
  gaussian model with the width equal to the design expectation.  This can be
  improved by something similar to our atmospheric model, and requires further
study.}


\item{{\bf Saturation and bleeding:}  The CCDs saturate at exactly 100,000 electrons
per pixel, and the excess charge is shifted to exactly the closest pixel.  In reality,
this is an ideal approximation and the charge can shift further, and become
saturated with some variance.  A study of bleed trails with prototype devices
can be used to come up with a better model.}

\item{{\bf Fringing:}  Fringing from strong emission lines in the sky background has been studied by Rasmussen, but not implemented
  in the simulator.  We could add this capability.}

\item{{\bf Study of ghosts:}  Ghost have been simulated, but not studied in great
  detail because they haven't been turned on in recent data challenges.  We
  expect to start simulating them in the near future.  Particular attention to
the coating design is important, because it produces a very different ghost
pattern.  There are computational challenges for this, but we have done
prototype testing. }

\item{{\bf Glints:}  Glints off certain edges of surfaces should be
  considered.}

\item {{\bf Photo-response non-uniformity (PRNU):}
This effect is not included and is especially important for u band.}

\item {{\bf Polarization:}
Photons enter the Camera from a wide range of angle (+- 22 degrees) so that the outcome
could dependent on the polarization which could effect the PSF.}

\item {{\bf Shutter model:}
The current model assumes that all pixels of all CCDs have the same start and stop times
for 15 second exposure. We need to model the shutter time dependence of correct the start and stop
times for each pixel of each CCD. The full details are known including
mechanical repetability, but the model is not yet implemented in the code.}

\item {{\bf Real flatness data for CCDs}
We current have a model that matches requirement but may not be physically reasonable.
There is some possibility that there is significant high frequency spatial
structure.  The flatness interface supports arbitrary shapes.}  


\item{{\bf Dust on surfaces:}  We have included only the coating reflectivity
  functions on all surfaces, but we could include models to degrade their
  reflectivity over time (or improve it through re-coating), or produce additional realism in their efficiency.}

\end{itemize}

\section{Non-astronomical source simulation improvements}

\begin{itemize}

\item{{\bf More components in background model:}  The background model
          consists of two components: the dark sky and the light reflected off
          the moon.  The model does not, however, self-consistently include
          the clouds used in the current atmosphere model nor the variation of
the emission components across the sky (although we do couple the water vapor
opacity to the normalization of the dark sky and use the average cloud
opacity).  Stray light from large angles is not included currently either.
Future improvements could be to accurately model the spatial variation, its
wavelength dependence, and time dependence of the actual sky.  Comparison to
real data, and models in the literature could provide more detail.}

\item{{\bf Dome light and dome screen model:}  We have approximated the dome light
  as a perfectly flat SED source and the dome screen as a perfectly uniform
  reflector.  We could update both of these models to produce more realistic
  dome flats.}

\item{{\bf Cosmic ray site data:}  A simple improvement to the cosmic ray model
  would be to use template images based on actual rays taken at the actual
  site.  The actual observed rate in rays per sq. centimeter can be used.  We
  also can use GEANT simulation to build a complete model including
  secondaries from the entire telescope structure.    It
  would also be an improvement to increase the library of cosmic rays images.}

\end{itemize}

\section{Readout physics improvements}

\begin{itemize}

        \item{{\bf Device design:}  There are two amplifier layout designs that also may
          change over time that we will need to update and track.  Further
          details, such as edge pixels, can be added as designs become more
          specific.}

          \item{{\bf Edge Pixels:}  An obvious change to the device design is
            to correctly describe the pixels at the very edge from either
            prototype devices or a model.}

		\item{{\bf Update to ADC modeling:}  We use a simple model based on a
          single equation to predict the conversion from electrons to ADU
          counts.  To enhance this model, we can include a position dependent
          offset, a more complex pre-scan/over-scan model based on actual
          electronic response, read noise/dark
          current predictions from actual devices, dark current pixel to pixel
          variation, gain non-linearity in the high and low signal range, ADC
          non-linearity, ADC digitization errors, and detector cross-talk
          (both neighboring amplifiers and external electronics cross-talk).}

		\item{{\bf Detector defect model:}  The current detector defects contains QE
          variation, hot pixels, hot columns, and dead pixels.  We have simple
          models for each of these.  We could improve these models from
          studying the rate of defects from actual prototype devices, as well
          as adding new models, such as edge pixels, and making these model
          time-dependent.  The hot pixel rates are temperature dependent, and
          that could be modelled with prototype-devices.  We could also
          include a detector failure rate, and an amplifier failure rate.}

\end{itemize}

\section{Further validation}

\begin{itemize}
     
         \item{{\bf More detailed alternative calculations:}  There are a variety of more detailed
           validation tests that can be done to compare with alternative
           calculations that can be done within the project.  This often
           involves simulating some specific configuration that can be
           calculated from an alternative code.  The ghost pattern from a
           bright star, for instance, would be an ideal more complex test.}

		\item{{\bf Validation and Studies with other telescopes:}  We have the prototype
          capability to simulate the Subaru telescope and the calibration 
          telescope and hopefully others soon.   The calibration telescope can
          help us to understand the common atmospheric model by simulating
          simultaneous LSST and calibration telescope observations.  These
          simulations will help to validate the simulator in much greater
          detail, and are also scientifically useful.  A
          number of other possibilities would help the project and help us
          with validation.  For example, we could simulate the Camera Calibration Optical
          Bench (CCOB), the photon counting calibration concept, the BNL CCD
          test system, or other optical systems. }



\end{itemize}
